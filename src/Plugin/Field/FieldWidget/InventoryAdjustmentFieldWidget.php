<?php

namespace Drupal\trinion_tp\Plugin\Field\FieldWidget;

use Drupal\Core\Field\FieldItemListInterface;
use Drupal\Core\Field\WidgetBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\node\Entity\Node;

/**
 * Виджет для отображения элементов перемещения товаров
 *
 * @FieldWidget(
 *   id = "trinion_tp_inventory_adjustment_field_widget",
 *   label = "Строки перемещения товаров",
 *   field_types = {"entity_reference"},
 * )
 */
class InventoryAdjustmentFieldWidget extends WidgetBase {

  /**
   * {@inheritdoc}
   */
  public function formElement(FieldItemListInterface $items, $delta, array $element, array &$form, FormStateInterface $form_state) {
    $stroka_uit = $items[$delta]->entity;
    $vals = $form_state->get('vals');
    if ($stroka_uit) {
      if (($stroka_uit_tovar = $items[$delta]->entity->get('field_tp_tovar')->first()) && isset($stroka_uit_tovar->entity)) {
        $stroka_uit_tovar_name = $stroka_uit_tovar->entity->label() . ' [' . $stroka_uit_tovar->entity->get('field_tp_artikul')->getString() . ']';
        $harakteristiki = \Drupal::service('trinion_tp.helper')->getHarakteristikiTovara($stroka_uit_tovar->entity);
        $harakteristika = $items[$delta]->entity->get('field_tp_kharakteristika_tovara')->getString();
      }
      else
        $stroka_uit_tovar_name = $stroka_uit->label();
    }

    $element['uit'] = [
      '#type' => 'textfield',
      '#autocomplete_route_name' => 'trinion_tp.tovar.autocomplete',
      '#title' => 'Товар',
      '#attributes' => ['class' => ['uit']],
      '#default_value' => $stroka_uit_tovar_name ?? '',
      '#size' => '255',
    ];
    if (\Drupal::config('trinion_tp.settings')->get('harakteristiki')) {
      $element['harakteristika'] = [
        '#type' => 'select',
        '#title' => t('Сharacteristics'),
        '#options' => $harakteristiki ?? [],
        '#attributes' => ['class' => ['harakteristika']],
        '#default_value' => $harakteristika ?? '',
        '#validated' => TRUE,
      ];
    }
    $element['kolichestvo_dostupno'] = [
      '#type' => 'number',
      '#title' => t('Quantity Available'),
      '#attributes' => ['class' => ['kolichestvo_dostupno'], 'readonly' => 'readonly'],
      '#step' => '0.01',
      '#default_value' => $stroka_uit ? $stroka_uit->get('field_tp_kolichestvo_dostupno')->getString() : '',
    ];
    $element['kolichestvo_na_rukah'] = [
      '#type' => 'number',
      '#title' => t('New Quantity on hand'),
      '#attributes' => ['class' => ['kolichestvo_na_rukah']],
      '#step' => '0.01',
      '#default_value' => $stroka_uit ? $stroka_uit->get('field_tp_kolichestvo_na_rukah')->getString() : '',
    ];
    $element['kolichestvo'] = [
      '#type' => 'number',
      '#title' => t('Quantity Adjusted'),
      '#attributes' => ['class' => ['kolichestvo']],
      '#step' => '0.01',
      '#default_value' => $stroka_uit ? $stroka_uit->get('field_tp_kolichestvo')->getString() : '',
      '#placeholder' => t('Eg. :example', [':example' => '+1, -1']),
    ];

    foreach (\Drupal::entityTypeManager()->getStorage('taxonomy_term')->loadTree('edinicy_izmereniya') as $term)
      $opts[$term->tid] = $term->name;
    $element['edinica_izmereniya'] = [
      '#type' => 'select',
      '#title' => 'Ед. изм.',
      '#options' => $opts,
      '#attributes' => ['class' => ['edinica_izmereniya']],
      '#default_value' => $stroka_uit ? $stroka_uit->get('field_tp_ed_izmereniya_prosl')->getString() : key($opts),
    ];
    $element['id'] = [
      '#type' => 'hidden',
      '#value' => is_object($stroka_uit) ? $stroka_uit->id() : ($vals[$delta]['target_id'] ?? ''),
    ];

    return $element;
  }

  public function massageFormValues(array $values, array $form, FormStateInterface $form_state) {
    static $vals = [];
    if ($vals)
      return $vals;
    if ($form_state->getErrors())
      return;
    else {
      $vals = [];
      foreach ($values as $item) {
        if (empty($item['kolichestvo']) || empty($item['uit']))
          continue;
        if (empty($item['id'])) {
          $data = [
            'type' => 'tp_stroka_dokumenta_uit',
            'title' => $item['uit'],
            'uid' => \Drupal::currentUser()->id(),
            'status' => 1,
            'field_tp_kolichestvo' => $item['kolichestvo'],
            'field_tp_kolichestvo_dostupno' => $item['kolichestvo_dostupno'],
            'field_tp_kolichestvo_na_rukah' => $item['kolichestvo_na_rukah'],
            'field_tp_edinica_izmereniya' => $item['edinica_izmereniya'] == '' ? NULL : $item['edinica_izmereniya'],
          ];
          if (!empty($item['harakteristika']))
            $data['field_tp_kharakteristika_tovara'] = $item['harakteristika'];
          if ($tovar = \Drupal::service('trinion_tp.helper')->tovarfromAutocomleteString($item['uit'])) {
            $data['field_tp_tovar'] = $tovar->id();
            $data['field_tp_artikul'] = $tovar->get('field_tp_artikul')->getString();
          }
          $stroka_uit = Node::create($data);
          $stroka_uit->save();
          $vals[] = ['target_id' => $stroka_uit->id()];
        }
        else {
          $tovar = \Drupal::service('trinion_tp.helper')->tovarfromAutocomleteString($item['uit']);
          $stroka_uit = Node::load($item['id']);
          $stroka_uit->field_tp_kolichestvo = $item['kolichestvo'];
          $stroka_uit->field_tp_kolichestvo_dostupno = $item['kolichestvo_dostupno'];
          $stroka_uit->field_tp_kolichestvo_na_rukah = $item['kolichestvo_na_rukah'];
          $stroka_uit->field_tp_edinica_izmereniya = $item['edinica_izmereniya'] == '' ? NULL : $item['edinica_izmereniya'];
          $stroka_uit->field_tp_kharakteristika_tovara = !empty($item['harakteristika']) ? $item['harakteristika'] : NULL;
          $stroka_uit->title = $item['uit'];
          if ($tovar) {
            $stroka_uit->field_tp_tovar = $tovar->id();
            $stroka_uit->field_tp_artikul = $tovar->get('field_tp_artikul')->getString();
            $stroka_uit->title = $tovar->label();
          }
          $stroka_uit->save();
          $vals[] = ['target_id' => $stroka_uit->id()];
        }
      }
    }

    $form_state->set('vals', $vals);
    return $vals;
  }
}
