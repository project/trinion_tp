<?php

namespace Drupal\trinion_tp\Plugin\Field\FieldWidget;

use Drupal\Core\Field\FieldItemListInterface;
use Drupal\Core\Field\WidgetBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\node\Entity\Node;

/**
 * Виджет для отображения элементов поступления товаров
 *
 * @FieldWidget(
 *   id = "trinion_tp_postuplenie_tovarov_field_widget",
 *   label = "Строки поступления товаров",
 *   field_types = {"entity_reference"},
 * )
 */
class PostuplenieTovarovFieldWidget extends WidgetBase {

  /**
   * {@inheritdoc}
   */
  public function formElement(FieldItemListInterface $items, $delta, array $element, array &$form, FormStateInterface $form_state) {
    $stroka_uit = $items[$delta]->entity;
    $vals = $form_state->get('vals');
    if ($stroka_uit) {
      if (($stroka_uit_tovar = $items[$delta]->entity->get('field_tp_tovar')->first()) && isset($stroka_uit_tovar->entity)) {
        $stroka_uit_tovar_name = $stroka_uit_tovar->entity->label() . ' [' . $stroka_uit_tovar->entity->get('field_tp_artikul')->getString() . ']';
        $harakteristiki = \Drupal::service('trinion_tp.helper')->getHarakteristikiTovara($stroka_uit_tovar->entity);
        $harakteristika = $items[$delta]->entity->get('field_tp_kharakteristika_tovara')->getString();
      }
      else
        $stroka_uit_tovar_name = $stroka_uit->label();
    }
    else {
      $values = $form_state->getUserInput();
      if (isset($values['field_tp_stroki'][$delta])) {
        $stroka_uit = $values['field_tp_stroki'][$delta];
        if ($stroka_uit['uit']) {
          $tovar = \Drupal::service('trinion_tp.helper')->tovarfromAutocomleteString($stroka_uit['uit']);
          $harakteristiki = \Drupal::service('trinion_tp.helper')->getHarakteristikiTovara($tovar);
        }
      }
    }

    $element['uit'] = [
      '#type' => 'textfield',
      '#autocomplete_route_name' => 'trinion_tp.tovar.autocomplete',
      '#title' => 'Товар',
      '#attributes' => ['class' => ['uit']],
      '#default_value' => $stroka_uit_tovar_name ?? '',
      '#size' => '255',
    ];
    if (\Drupal::config('trinion_tp.settings')->get('harakteristiki')) {
      $element['harakteristika'] = [
        '#type' => 'select',
        '#title' => t('Сharacteristics'),
        '#options' => $harakteristiki ?? [],
        '#attributes' => ['class' => ['harakteristika']],
        '#default_value' => $harakteristika ?? '',
        '#validated' => TRUE,
      ];
    }
    $element['kolichestvo'] = [
      '#type' => 'number',
      '#title' => 'Кол-во',
      '#attributes' => ['class' => ['kolichestvo']],
      '#step' => '0.01',
      '#default_value' => is_object($stroka_uit) ? $stroka_uit->get('field_tp_kolichestvo')->getString() : '',
    ];
    $element['cena'] = [
      '#type' => 'number',
      '#title' => 'Цена',
      '#attributes' => ['class' => ['cena']],
      '#step' => '0.01',
      '#default_value' => is_object($stroka_uit) ? $stroka_uit->get('field_tp_cena')->getString() : '0',
    ];
    $opts = [];
    foreach (\Drupal::entityTypeManager()->getStorage('taxonomy_term')->loadTree('edinicy_izmereniya') as $term)
      $opts[$term->tid] = $term->name;
    $element['edinica_izmereniya'] = [
      '#type' => 'select',
      '#title' => t('Unit'),
      '#options' => $opts,
      '#attributes' => ['class' => ['edinica_izmereniya']],
      '#default_value' => is_object($stroka_uit) ? $stroka_uit->get('field_tp_edinica_izmereniya')->getString() : key($opts),
    ];
    $field_definitions = \Drupal::service('entity_field.manager')->getFieldDefinitions('node', 'tp_stroka_dokumenta_uit');
    $opts = $field_definitions['field_tp_nds']->getSetting('allowed_values');
    $element['nds'] = [
      '#type' => 'select',
      '#title' => 'НДС',
      '#options' => $opts,
      '#attributes' => ['class' => ['nds']],
      '#default_value' => is_object($stroka_uit) ? $stroka_uit->get('field_tp_nds')->getString() : key($opts),
    ];
    $element['itogo'] = [
      '#type' => 'hidden',
      '#attributes' => ['class' => ['itogo']],
      '#default_value' => is_object($stroka_uit) ? $stroka_uit->get('field_tp_itogo')->getString() : '',
    ];
    $element['id'] = [
      '#type' => 'hidden',
      '#value' => is_object($stroka_uit) ? $stroka_uit->id() : ($vals[$delta]['target_id'] ?? ''),
    ];

    return $element;
  }

  public function massageFormValues(array $values, array $form, FormStateInterface $form_state) {
    static $vals = [];
    if ($vals)
      return $vals;
    if ($form_state->getErrors())
      return;
    else {
      $vals = [];
      foreach ($values as $item) {
        if (empty($item['kolichestvo']) || empty($item['uit']))
          continue;
        if (empty($item['id'])) {
          $data = [
            'type' => 'tp_stroka_dokumenta_uit',
            'title' => $item['uit'],
            'uid' => \Drupal::currentUser()->id(),
            'status' => 1,
            'field_tp_kolichestvo' => $item['kolichestvo'],
            'field_tp_edinica_izmereniya' => $item['edinica_izmereniya'],
            'field_tp_cena' => $item['cena'],
            'field_tp_itogo' => $item['itogo'],
            'field_tp_nds' => $item['nds'],
          ];
          if (!empty($item['harakteristika']))
            $data['field_tp_kharakteristika_tovara'] = $item['harakteristika'];
          if ($tovar = \Drupal::service('trinion_tp.helper')->tovarfromAutocomleteString($item['uit'])) {
            $data['field_tp_tovar'] = $tovar->id();
            $data['field_tp_artikul'] = $tovar->get('field_tp_artikul')->getString();
          }
          $stroka_uit = Node::create($data);
          $stroka_uit->save();
          $vals[] = ['target_id' => $stroka_uit->id()];
        }
        else {
          if ($form_state->getValue('field_tp_stroki_add_more')) {
            $stroka_id = $item['id'];
          }
          else {
            $tovar = \Drupal::service('trinion_tp.helper')->tovarfromAutocomleteString($item['uit']);
            $stroka_uit = Node::load($item['id']);
            $stroka_uit->field_tp_cena = $item['cena'];
            $stroka_uit->field_tp_kolichestvo = $item['kolichestvo'];
            $stroka_uit->field_tp_edinica_izmereniya = $item['edinica_izmereniya'];
            $stroka_uit->field_tp_itogo = $item['itogo'];
            $stroka_uit->field_tp_nds = $item['nds'];
            $stroka_uit->title = $item['uit'];
            $stroka_uit->field_tp_kharakteristika_tovara = !empty($item['harakteristika']) ? $item['harakteristika'] : NULL;
            if ($tovar) {
              $stroka_uit->field_tp_tovar = $tovar->id();
              $stroka_uit->field_tp_artikul = $tovar->get('field_tp_artikul')->getString();
              $stroka_uit->title = $tovar->label();
            }
            $stroka_uit->save();
            $stroka_id = $stroka_uit->id();
          }
          $vals[] = ['target_id' => $stroka_id];
        }
      }
    }
    $form_state->set('vals', $vals);
    return $vals;
  }

  public function uitValidate($form, FormStateInterface $form_state) {
    $stroki = $form_state->getValues()['field_tp_stroki'];
    foreach ($stroki as $key => $item) {
      if (is_numeric($key) && $item['uit'] != '') {
        $tovar = \Drupal::service('trinion_tp.helper')->tovarfromAutocomleteString($item['uit']);
        if (!$tovar) {
          $form_state->setErrorByName('', 'Не корректно выбрана услуга или товар в строке ' . $key + 1);
        }
      }
    }
  }
}
