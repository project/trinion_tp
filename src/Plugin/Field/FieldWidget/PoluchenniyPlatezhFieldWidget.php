<?php

namespace Drupal\trinion_tp\Plugin\Field\FieldWidget;

use Drupal\Core\Field\FieldItemListInterface;
use Drupal\Core\Field\WidgetBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\node\Entity\Node;

/**
 * Виджет для отображения счетов полученного платежа
 *
 * @FieldWidget(
 *   id = "trinion_tp_poluchenniy_platezh_field_widget",
 *   label = "Строки полученного платежа",
 *   field_types = {"entity_reference"},
 * )
 */
class PoluchenniyPlatezhFieldWidget extends WidgetBase {

  /**
   * {@inheritdoc}
   */
  public function formElement(FieldItemListInterface $items, $delta, array $element, array &$form, FormStateInterface $form_state) {
    if ($platezh = \Drupal::routeMatch()->getParameter('node')) {
      $companiya = $platezh->get('field_tp_platelschik')->getString();
    }
    else {
      $comp = \Drupal::request()->get('field_tp_platelschik');
      $companiya = NULL;
      if (isset($comp['0']['target_id']) && preg_match('/\((\d+)\)("|)$/', $comp['0']['target_id'], $match)) {
        $companiya = $match[1];
      }
    }
    $scheta = \Drupal::service('trinion_tp.helper')->schetaCompaniiDliaOplati($companiya, $platezh, 'schet');

    foreach ($scheta as $schet_id) {
      if ($schet = Node::load($schet_id)) {
        $element[$schet_id]['data'] = [
          '#type' => 'markup',
          '#markup' => date('j.m.Y', strtotime($schet->get('field_tp_data')->getString())),
        ];
        $element[$schet_id]['nomer'] = [
          '#type' => 'markup',
          '#markup' => $schet->label(),
        ];
        $summa = $schet->get('field_tp_itogo')->getString();
        $schet_data = \Drupal::service('trinion_tp.helper')->calculateSchetKlienta($schet);
        $oplachenaya_summa = $schet_data['summa_oplati'];
        if (!is_numeric($oplachenaya_summa))
          $oplachenaya_summa = 0;
        $element[$schet_id]['summa'] = [
          '#type' => 'markup',
          '#markup' => $summa,
        ];
        $neoplacheno = $summa - $oplachenaya_summa;
        $element[$schet_id]['ne_oplacheno'] = [
          '#type' => 'markup',
          '#markup' => $neoplacheno,
        ];
        $element[$schet_id]['nid'] = [
          '#type' => 'hidden',
          '#value' => $schet_id,
        ];
        $element[$schet_id]['oplacheno'] = [
          '#type' => 'number',
          '#default_value' => 0,
          '#size' => 10,
          '#step' => '0.01',
          '#max' => $summa,
        ];
        $element[$schet_id]['stroka_id'] = [
          '#type' => 'hidden',
          '#default_value' => '',
        ];
        foreach ($items as $item) {
          if (is_null($item->entity))
            continue;
          $schet_saved = Node::load($item->entity->get('field_tp_schet')->getString());
          if ($schet_saved->id() == $schet_id) {
            $element[$schet_id]['oplacheno']['#default_value'] = $item->entity->get('field_tp_oplachennaya_summa')->getString();
            $element[$schet_id]['stroka_id']['#default_value'] = $item->entity->id();
          }
        }
      }
    }
    return $element;
  }

  public function massageFormValues(array $values, array $form, FormStateInterface $form_state) {
    static $vals = [];
    if ($vals)
      return $vals;
    if ($form_state->getErrors())
      return;
    else {
      $element = $form_state->getTriggeringElement();
      if (strpos($element['#name'], 'field_tp_platelschik') === 0)
        return;
      $post_data = \Drupal::request()->request->all();
      $itogo = $post_data['field_tp_itogo'][0]['value'];
      foreach ($post_data['field_tp_stroki'][0] as $schet_nid => $schet_data) {
        $itogo -= (float)$schet_data['oplacheno'];
      }
      if ($itogo < 0) {
        $form_state->setErrorByName('', 'Оплаченная сумма счетов не может превышать сумму платежа. Разница ' . abs($itogo) . ' руб.');
        return;
      }


      foreach ($values[0] as $key => $schet) {
        if (!is_numeric($key) || empty($schet['oplacheno']))
          continue;
        if ($node_schet = Node::load($schet['nid'])) {
          if (empty($schet['stroka_id'])) {
            $data = [
              'type' => 'tp_stroka_schet_klienta_k_oplate',
              'title' => $node_schet->label(),
              'uid' => \Drupal::currentUser()->id(),
              'status' => 1,
              'field_tp_schet' => $schet['nid'],
              'field_tp_oplachennaya_summa' => $schet['oplacheno'],
            ];
            $stroka_schet = Node::create($data);
          }
          else {
            $stroka_schet = Node::load($schet['stroka_id']);
            $stroka_schet->field_tp_oplachennaya_summa = $schet['oplacheno'];
          }
          $stroka_schet->save();
          $vals[] = ['target_id' => $stroka_schet->id()];
        }
      }
    }
    return $vals;
  }
}
