<?php

namespace Drupal\trinion_tp\Plugin\views\field;

use Drupal\views\Plugin\views\field\FieldPluginBase;
use Drupal\views\ResultRow;

/**
 * Provides SchetKlientaSostoyanieOplati field handler.
 *
 * @ViewsField("trinion_tp_schet_klienta_sostoyanie_oplati")
 */
class SchetKlientaSostoyanieOplati extends FieldPluginBase {

  public function advancedRender(ResultRow $values) {
    $schet_data = \Drupal::service('trinion_tp.helper')->calculateSchetKlienta($values->_entity);
    return [
      '#state_id' => $schet_data['sostoyanie_oplati_tid'],
      '#markup' => $schet_data['sostoyanie_oplati'],
    ];
  }

  public function query() {

  }
}
