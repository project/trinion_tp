<?php

namespace Drupal\trinion_tp\Plugin\views\field;

use Drupal\views\Plugin\views\field\FieldPluginBase;
use Drupal\views\ResultRow;

/**
 * Provides SchetPostavshikaPayedSum field handler.
 *
 * @ViewsField("trinion_tp_schet_postavshika_oplacheno")
 */
class SchetPostavshikaPayedSum extends FieldPluginBase {

  /**
   * {@inheritdoc}
   */
  public function render(ResultRow $values) {
    $schet_data = \Drupal::service('trinion_tp.helper')->calculateSchetProdavtsa($values->_entity);
    return $schet_data['summa_oplati'];
  }

  public function query() {

  }

}
