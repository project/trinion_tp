<?php

namespace Drupal\trinion_tp\Plugin\views\field;

use Drupal\views\Plugin\views\field\FieldPluginBase;
use Drupal\views\ResultRow;

/**
 * Provides JivoyOstatokTovara field handler.
 *
 * @ViewsField("trinion_tp_jivoy_ostatok_tovara")
 */
class JivoyOstatokTovara extends FieldPluginBase {

  /**
   * {@inheritdoc}
   */
  public function render(ResultRow $values) {
    $tovar_nid = $values->_entity->get('field_tp_tovar')->getString();
    $sklad_tid = $values->_entity->get('field_tp_sklad')->getString();
    $harakteristika_tid = $values->_entity->get('field_tp_kharakteristika_tovara')->getString();
    $edinica_izmereniya_tid = $values->_entity->get('field_tp_edinica_izmereniya')->getString();
    return \Drupal::service('trinion_tp.helper')->getLiveOstatok($tovar_nid, $sklad_tid, $harakteristika_tid, $edinica_izmereniya_tid);
  }

  public function query() {

  }

}
