<?php

namespace Drupal\trinion_tp\Plugin\views\field;

use Drupal\views\Plugin\views\field\FieldPluginBase;
use Drupal\views\ResultRow;

/**
 * Provides SchetPostavshikaSostoyanieOplati field handler.
 *
 * @ViewsField("trinion_tp_schet_postavshika_sostoyanie_oplati")
 */
class SchetPostavshikaSostoyanieOplati extends FieldPluginBase {

  public function advancedRender(ResultRow $values) {
    $schet_data = \Drupal::service('trinion_tp.helper')->calculateSchetProdavtsa($values->_entity);
    return [
      '#state_id' => $schet_data['sostoyanie_oplati_tid'],
      '#markup' => $schet_data['sostoyanie_oplati'],
    ];
  }

  public function query() {

  }
}
