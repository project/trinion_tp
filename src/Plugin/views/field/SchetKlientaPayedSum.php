<?php

namespace Drupal\trinion_tp\Plugin\views\field;

use Drupal\views\Plugin\views\field\FieldPluginBase;
use Drupal\views\ResultRow;

/**
 * Provides SchetKlientaPayedSum field handler.
 *
 * @ViewsField("trinion_tp_schet_klienta_oplacheno")
 */
class SchetKlientaPayedSum extends FieldPluginBase {

  /**
   * {@inheritdoc}
   */
  public function render(ResultRow $values) {
    $schet_data = \Drupal::service('trinion_tp.helper')->calculateSchetKlienta($values->_entity);
    return $schet_data['summa_oplati'];
  }

  public function query() {

  }

}
