<?php

namespace Drupal\trinion_tp\Plugin\views\field;

use Drupal\Core\Form\FormStateInterface;
use Drupal\views\Plugin\views\field\FieldPluginBase;
use Drupal\views\ResultRow;

/**
 * Provides ZakazKlientaOtgruzkaStatus field handler.
 *
 * @ViewsField("trinion_tp_schet_otgruzka_status")
 */
class ZakazKlientaOtgruzkaStatus extends FieldPluginBase {

  protected function defineOptions() {
    $options = parent::defineOptions();
    $options['formatter'] = ['default' => 'label'];
    return $options;
  }

  public function buildOptionsForm(&$form, FormStateInterface $form_state) {
    $form['formatter'] = [
      '#type' => 'select',
      '#title' => 'Formatter',
      '#options' => [
        'label' => 'label',
        'id' => 'id',
      ],
      '#default_value' => $this->options['formatter'],
    ];
    parent::buildOptionsForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function render(ResultRow $values) {
    return $this->options['formatter'] == 'label' ? $values->_entity->otgruzka_status : $values->_entity->otgruzka_status_id;
  }

  public function query() {

  }

}
