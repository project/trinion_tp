<?php

declare(strict_types=1);

namespace Drupal\trinion_tp\Plugin\EntityReferenceSelection;

use Drupal\Core\Entity\Query\QueryInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\node\Entity\Node;
use Drupal\taxonomy\Entity\Term;
use Drupal\taxonomy\Plugin\EntityReferenceSelection\TermSelection;

/**
 * @todo Add plugin description here.
 *
 * @EntityReferenceSelection(
 *   id = "trinion_tp_characteristics_by_project_selection",
 *   label = @Translation("Characteristics by project selection"),
 *   group = "trinion_tp_characteristics_by_project_selection",
 *   entity_types = {"taxonomy_term"},
 * )
 */
final class CharacteristicsByProductSelection extends TermSelection {
  /**
   * {@inheritdoc}
   */
  protected function buildEntityQuery($match = NULL, $match_operator = 'CONTAINS'): QueryInterface {
    $query = parent::buildEntityQuery($match, $match_operator);
    return $query;
  }

  public function buildConfigurationForm(array $form, FormStateInterface $form_state) {
    return [
      '#markup' => t('Work only for Items'),
    ];
  }

  public function getReferenceableEntities($match = NULL, $match_operator = 'CONTAINS', $limit = 0) {
    if (($item = \Drupal::request()->get('field_tp_tovar')) && preg_match('/\((\d+)\)("|)\s*$/', $item[0]['target_id'], $match)) {
      $item_id = $match[1];
    }
    else {
      $route_match = \Drupal::routeMatch();
      $route_name = $route_match->getRouteName();
      if ($route_name == 'entity.node.edit_form') {
        $node = $route_match->getParameter('node');
        if ($item = Node::load($node->get('field_tp_tovar')->getString()))
          $item_id = $item->id();
      }
    }
    if (!empty($item_id)) {
      $tovar = Node::load($item_id);
      $response = \Drupal::service('trinion_tp.helper')->getHarakteristikiTovara($tovar, FALSE, FALSE);
      return ['kharakteristiki_tovara' => $response];
    }
    return [];
  }
}
