<?php

namespace Drupal\trinion_tp\Access;

use Drupal\Core\Access\AccessResult;
use Drupal\Core\Routing\Access\AccessInterface;
use Drupal\node\Entity\Node;
use Symfony\Component\Routing\Route;

/**
 * Проверка доступа к утверждению документа
 */
class UtverditDokumentChecker implements AccessInterface {

  /**
   * Access callback.
   */
  public function access(Route $route, $node, $op) {
    $node = Node::load($node);
    $bundle = $node->bundle();
    if ($node->hasField('field_tp_otvetstvennyy') &&
      $node->hasField('field_tp_utverzhdeno') &&
      (
        $bundle == 'zakaz_klienta' &&
        $node->get('field_tp_zarezervirovan')->getString() != $op
        ||
        $node->get('field_tp_utverzhdeno')->getString() != $op
      )
    ) {
      $user = \Drupal::currentUser();
      if ($user->hasPermission('trinion_base edit all'))
        return AccessResult::allowed();
      if ($user->id() == $node->get('field_tp_otvetstvennyy')->getString())
        return AccessResult::allowed();
    }
    return AccessResult::forbidden();
  }
}
