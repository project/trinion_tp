<?php

namespace Drupal\trinion_tp;

use Drupal\Core\Database\Connection;
use Drupal\Core\Entity\EntityTypeBundleInfo;
use Drupal\Core\Entity\EntityTypeManager;
use Drupal\node\Entity\Node;
use Drupal\taxonomy\Entity\Term;
use Drupal\user\Entity\User;

class TrinionHelper {

  /**
   * The database connection.
   *
   * @var \Drupal\Core\Database\Connection
   */
  protected $connection;

  /**
   * Constructs a TrinionHelper object.
   *
   * @param \Drupal\Core\Database\Connection $connection
   *   The database connection.
   */
  public function __construct(Connection $connection) {
    $this->connection = $connection;
  }

  /**
   * Получение товара из строки поиска autocomplete
   */
  public function tovarfromAutocomleteString($str) {
    if (preg_match('/\s\[(.+)\]$/U', $str, $match)) {
      $query = \Drupal::entityQuery('node')
        ->condition('type', \Drupal::service('trinion_tp.helper')->getProductBundles(), 'IN')
        ->condition('field_tp_artikul', $match[1]);
      if ($res = $query->accessCheck()->execute()) {
        return Node::load(reset($res));
      }
    }
  }

  /**
   * Подсчет итоговых значений скидки, наценки, налога и итого по документу
   * @param $entity
   */
  public function calculateTotals($entity) {
    $nacenka_itog = 0;
    $skidka_itog = 0;
    $nds_itog = 0;
    $nds_abs = 0;
    $cena_itog = 0;
    foreach ($entity->field_tp_stroki as $stroka) {
      $stroka = $stroka->entity;
      $kolichestvo = $stroka->get('field_tp_kolichestvo')->getString();
      $cena = $stroka->get('field_tp_cena')->getString() * $kolichestvo;
      $nacenka_abs = 0;
      $skidka_abs = 0;
      if ($nacenka = floatval($stroka->get('field_tp_nacenka')->getString())) {
        $nacenka_abs = ($cena / 100) * $nacenka;
      }
      $nacenka_summa = floatval($stroka->get('field_tp_nacenka_summa')->getString());
      $nacenka_abs += $nacenka_summa;
      if ($skidka = floatval($stroka->get('field_tp_skidka')->getString())) {
        $skidka_abs = ($cena / 100) * $skidka;
      }
      $skidka_summa = floatval($stroka->get('field_tp_skidka_summa')->getString());
      $skidka_abs += $skidka_summa;
      $cena += $nacenka_abs - $skidka_abs;

      if ($nds = floatval($stroka->get('field_tp_nds')->getString())) {
        $nds_abs = ($cena / (100 + $nds)) * $nds;
      }
      else
        $nds_abs = 0;

      $nds_itog += $nds_abs;
      $nacenka_itog += $nacenka_abs;
      $skidka_itog += $skidka_abs;
      $cena_itog += $cena;
    }
    return [
      'nds_itog' => $nds_itog,
      'nacenka_itog' => $nacenka_itog,
      'skidka_itog' => $skidka_itog,
      'cena_itog' => $cena_itog,
    ];
  }

  /**
   * Получение Заказа покупателя привязанного к Коммерческому предложению
   */
  public function zakazKlientaFromKommPredlozhenie($nid) {
    $query = $this->connection->select('node__field_tp_komm_predlozhenie', 'k')
      ->condition('k.bundle', 'zakaz_klienta')
      ->condition('k.field_tp_komm_predlozhenie_target_id', $nid)
      ->range(0, 1);
    $query->addField('k', 'entity_id');
    return $query->execute()->fetchField();
  }

  /**
   * Получение Счета покупателя привязанного к Заказу клиента
   */
  public function schetKlientaFromZakazKlienta($nid) {
    $query = $this->connection->select('node__field_tp_zakaz_klienta', 'z')
      ->condition('z.field_tp_zakaz_klienta_target_id', $nid)
      ->condition('z.bundle', 'schet');
    $query->addField('z', 'entity_id');
    $res = [];
    foreach ($query->execute() as $row)
      $res[] = $row->entity_id;
    return $res;
  }

  /**
   * Get Payment received from Invoice
   */
  public function poluchenniyPlatezhFromSchetKlienta($nid) {
    $query = $this->connection->select('node__field_tp_schet', 'schet')
      ->condition('schet.bundle', 'tp_stroka_schet_klienta_k_oplate')
      ->condition('schet.field_tp_schet_target_id', $nid);
    $query->join('node__field_tp_stroki', 'stroka', 'stroka.field_tp_stroki_target_id = schet.entity_id');
    $query->addField('stroka', 'entity_id');
    $res = [];
    foreach ($query->execute() as $row)
      $res[] = $row->entity_id;
    return $res;
  }

  /**
   * Получение Отгрузок привязанных к Заказу покупателя
   */
  public function otgruzkiByZakazKlienta($nid) {
    $query = \Drupal::entityQuery('node')
      ->condition('type', 'otgruzka')
      ->condition('field_tp_zakaz_klienta', $nid)
      ->condition('field_tp_utverzhdeno', 1);
    return $query->accessCheck()->execute();
  }

  /**
   * Получение не отгруженных товаров из заказа клиента
   * @param $nid
   * @return void
   */
  public function neOtgruzhenoIzZakazKlienta($zakaz_klienta, $otgruzki) {
    $stroki_zakaza = [];
    foreach ($zakaz_klienta->get('field_tp_stroki') as $stroka_uit) {
      $stroka_uit = $stroka_uit->entity;
      $kolichestvo = $stroka_uit->get('field_tp_kolichestvo')->getString();
      $tovar_nid = $stroka_uit->get('field_tp_tovar')->getString();
      $sklad_tid = $stroka_uit->get('field_tp_sklad')->getString();
      $edinica_izereniya_tid = $stroka_uit->get('field_tp_edinica_izmereniya')->getString();
      $harakteristika_tid = $stroka_uit->get('field_tp_kharakteristika_tovara')->getString();
      $stroki_zakaza["{$tovar_nid}-{$sklad_tid}-{$edinica_izereniya_tid}-{$harakteristika_tid}"] = $kolichestvo;
    }
    foreach ($otgruzki as $otgruzka_nid) {
      $otgruzka = Node::load($otgruzka_nid);
      $sklad_tid = $otgruzka->get('field_tp_sklad')->getString();
      foreach ($otgruzka->get('field_tp_stroki') as $stroka_uit) {
        $stroka_uit = $stroka_uit->entity;
        $kolichestvo = $stroka_uit->get('field_tp_kolichestvo')->getString();
        $tovar_nid = $stroka_uit->get('field_tp_tovar')->getString();
        $edinica_izereniya_tid = $stroka_uit->get('field_tp_edinica_izmereniya')->getString();
        $harakteristika_tid = $stroka_uit->get('field_tp_kharakteristika_tovara')->getString();
        $key = "{$tovar_nid}-{$sklad_tid}-{$edinica_izereniya_tid}-{$harakteristika_tid}";
        if (isset($stroki_zakaza[$key])) {
          $stroki_zakaza[$key] -= $kolichestvo;
        }
      }
    }
    foreach ($stroki_zakaza as $key => $kolichestvo) {
      if (!$kolichestvo)
        unset($stroki_zakaza[$key]);
    }
    return $stroki_zakaza;
  }

  /**
   * Получение не отгруженных товаров из заказа поставщику
   * @param $nid
   * @return void
   */
  public function neOtgruzhenoIzZakazPostavshiku($zakaz_postavshiku, $postupleniya) {
    $stroki_postupleniya = [];
    foreach ($zakaz_postavshiku->get('field_tp_stroki') as $stroka_uit) {
      $stroka_uit = $stroka_uit->entity;
      $kolichestvo = $stroka_uit->get('field_tp_kolichestvo')->getString();
      $tovar_nid = $stroka_uit->get('field_tp_tovar')->getString();
      $edinica_izereniya_tid = $stroka_uit->get('field_tp_edinica_izmereniya')->getString();
      $harakteristika_tid = $stroka_uit->get('field_tp_kharakteristika_tovara')->getString();
      $stroki_postupleniya["{$tovar_nid}-{$edinica_izereniya_tid}-{$harakteristika_tid}"] = $kolichestvo;
    }
    foreach ($postupleniya as $postuplenie_nid) {
      $otgruzka = Node::load($postuplenie_nid);
      foreach ($otgruzka->get('field_tp_stroki') as $stroka_uit) {
        $stroka_uit = $stroka_uit->entity;
        if (is_null($stroka_uit))
          continue;
        $kolichestvo = $stroka_uit->get('field_tp_kolichestvo')->getString();
        $tovar_nid = $stroka_uit->get('field_tp_tovar')->getString();
        $edinica_izereniya_tid = $stroka_uit->get('field_tp_edinica_izmereniya')->getString();
        $harakteristika_tid = $stroka_uit->get('field_tp_kharakteristika_tovara')->getString();
        $key = "{$tovar_nid}-{$edinica_izereniya_tid}-{$harakteristika_tid}";
        if (isset($stroki_postupleniya[$key])) {
          $stroki_postupleniya[$key] -= $kolichestvo;
        }
      }
    }
    foreach ($stroki_postupleniya as $key => $kolichestvo) {
      if (!$kolichestvo)
        unset($stroki_postupleniya[$key]);
    }
    return $stroki_postupleniya;
  }

  /**
   * Получение Коммерческого предложения привязанного к Сделке
   */
  public function KPFromSdelka($nid) {
    $query = $this->connection->select('node__field_tp_sdelka', 'z')
      ->condition('z.field_tp_sdelka_target_id', $nid)
      ->condition('z.bundle', 'kommercheskoe_predlozhenie')
      ->range(0, 1);
    $query->addField('z', 'entity_id');
    return $query->execute()->fetchField();
  }

  /**
   * Получение Счета поставщика привязанного к Заказу поставщика
   */
  public function schetPostavshikaFromZakazPostavshika($nid) {
    $query = $this->connection->select('node__field_tp_zakaz_postavschika', 'z')
      ->condition('z.field_tp_zakaz_postavschika_target_id', $nid)
      ->condition('z.bundle', 'schet_postavschika')
      ->range(0, 1);
    $query->addField('z', 'entity_id');
    return $query->execute()->fetchField();
  }

  /**
   * Получение Поступлений товаров привязанных к Заказу поставщика
   */
  public function postupleniyeTovarovFromZakazPostavshika($nid) {
    $query = $this->connection->select('node__field_tp_zakazy_postavschiku', 'z')
      ->condition('z.field_tp_zakazy_postavschiku_target_id', $nid)
      ->condition('z.bundle', 'postuplenie_tovarov');
    $query->addField('z', 'entity_id');
    return $query->execute()->fetchCol();
  }

  /**
   * Получение Заказа поставщику привязанного к Заказу покупателя
   */
  public function zakazPostavshikuFromZakazKlienta($nid) {
    $query = $this->connection->select('node__field_tp_zakaz_klienta', 'z')
      ->condition('z.bundle', 'zakaz_postavschiku')
      ->condition('z.field_tp_zakaz_klienta_target_id', $nid)
      ->range(0, 1);
    $query->addField('z', 'entity_id');
    return $query->execute()->fetchField();
  }

  /**
   * Получение счетов компании для оплаты
   */
  public function schetaCompaniiDliaOplati($companiya, $platezh, $type) {
    $query = \Drupal::entityQuery('node');
    $or = $query->orConditionGroup();
    $query->condition('type', $type);
    $query->condition('field_tp_utverzhdeno', 1);
    $or->condition('field_tp_oplachen', 0);
    $or->notExists('field_tp_oplachen');
    if ($platezh) {
      $scheta_platezha = [];
      foreach ($platezh->get('field_tp_stroki') as $item) {
        if ($item->entity)
          $scheta_platezha[] = $item->entity->get('field_tp_schet')->first()->getString();
      }
      if ($scheta_platezha)
        $or->condition('nid', $scheta_platezha, 'IN');
    }
    $query->condition($or);
    $query->condition('field_tp_schet_dlya', $companiya);
    return $query->accessCheck()->execute();
  }

  /**
   * Получение следующего номера документа
   */
  public function getNextDocumentNumber($type) {
    $start_nomer = \Drupal::config('trinion_tp.settings')->get($type . '_start_nomer');
    if (is_null($start_nomer))
      $start_nomer = 1;
    $query = $this->connection->select('node_field_data', 'n')
      ->condition('n.type', $type);
    $query->addField('n', 'title');
    $query->addExpression('CAST (n.title AS UNSIGNED)', 't');
    $query->orderBy('t', 'DESC');
    $query->range(0, 1);
    $res = $query->execute()->fetchField();
    return !$res || $res < $start_nomer ? $start_nomer : $res + 1;
  }

  public function getPluralMonths($num) {
    $monthsList = [
      "1"=>"января",
      "2"=>"февраля",
      "3"=>"марта",
      "4"=>"апреля",
      "5"=>"мая",
      "6"=>"июня",
      "7"=>"июля",
      "8"=>"августа",
      "9"=>"сентября",
      "10"=>"октября",
      "11"=>"ноября",
      "12"=>"декабря"
    ];

    if($num > 0 &&  $num <= 12)
      return $monthsList[$num];

    return '';
  }

  public function getPluralDate($date) {
    if (empty($date))
      return '';
    $timestamp = strtotime($date);
    $day = date('j', $timestamp);
    $month = date('n', $timestamp);
    $month = $this->getPluralMonths($month);
    $year = date('Y', $timestamp);
    return "{$day} {$month} {$year}";
  }

  /**
   * Обновление статуса и прочих данных о Счете покупателя
   */
  public function updateSchet($scheta_to_recalc, $oplata_id, $op) {
    foreach ($scheta_to_recalc as $stroka_id) {
      $query = \Drupal::database()->select('node__field_tp_schet', 's')
        ->condition('s.bundle', 'tp_stroka_schet_klienta_k_oplate')
        ->condition('s.field_tp_schet_target_id', $stroka_id);
      $query->join('node__field_tp_oplachennaya_summa', 'o', 'o.entity_id = s.entity_id');
      $query->join('node__field_tp_stroki', 'str', 'str.field_tp_stroki_target_id = s.entity_id');
      $query->leftJoin('node__field_tp_utverzhdeno', 'paltezh_utverzhden', 'paltezh_utverzhden.entity_id = str.entity_id');
      $query->condition('paltezh_utverzhden.field_tp_utverzhdeno_value', 1);
      $query->addExpression('SUM(o.field_tp_oplachennaya_summa_value)');
      $sum = $query->execute()->fetchField();

      if ($schet = \Drupal\node\Entity\Node::load($stroka_id)) {
        $schet_itogo = $schet->get('field_tp_itogo')->getString();
        if ($sum == $schet_itogo)
          $schet->field_tp_oplachen = 1;
        else
          $schet->field_tp_oplachen = 0;
        $items = [];
        foreach ($schet->field_tp_oplati as $item)
          $items[] = $item->getString();
        if ($op == 'add' && !in_array($oplata_id, $items))
          $items[] = $oplata_id;
        if ($op == 'delete') {
          $key = array_search($oplata_id, $items);
          if (is_numeric($key)) {
            unset($items[$key]);
            $items = array_values($items);
          }
        }
        $schet->set('field_tp_oplati', $items);
        $schet->save();
      }
    }
  }

  /**
   * Обновление статуса и прочих данных о Счете продавца
   */
  public function updateSchetProdavtsa($scheta_to_recalc, $oplata_id, $op) {
    foreach ($scheta_to_recalc as $stroka_id) {
      $query = \Drupal::database()->select('node__field_tp_schet', 's')
        ->condition('s.bundle', 'stroka_scheta_prodavca_k_oplate')
        ->condition('s.field_tp_schet_target_id', $stroka_id);
      $query->join('node__field_tp_roditelskiy_dokument', 'p', 'p.entity_id = s.entity_id');
      $query->join('node__field_tp_utverzhdeno', 'u', 'u.entity_id = p.field_tp_roditelskiy_dokument_target_id');
      $query->condition('u.field_tp_utverzhdeno_value', 1);
      $query->join('node__field_tp_oplachennaya_summa', 'o', 'o.entity_id = s.entity_id');
      $query->addExpression('SUM(o.field_tp_oplachennaya_summa_value)');
      $sum = $query->execute()->fetchField();

      if ($schet = \Drupal\node\Entity\Node::load($stroka_id)) {
        $schet_itogo = $schet->get('field_tp_itogo')->getString();
        if ($sum == $schet_itogo)
          $schet->field_tp_oplachen = 1;
        else
          $schet->field_tp_oplachen = 0;
        $items = [];
        foreach ($schet->field_tp_oplati as $item)
          $items[] = $item->getString();
        if ($op == 'add' && !in_array($oplata_id, $items))
          $items[] = $oplata_id;
        if ($op == 'delete') {
          $key = array_search($oplata_id, $items);
          if (is_numeric($key)) {
            unset($items[$key]);
            $items = array_values($items);
          }
        }
        $schet->set('field_tp_oplati', $items);
        $schet->save();
      }
    }
  }

  public function calculateSchetKlienta($schet) {
    $query = \Drupal::database()->select('node__field_tp_schet', 's')
      ->condition('s.bundle', 'tp_stroka_schet_klienta_k_oplate')
      ->condition('s.field_tp_schet_target_id', $schet->id());
    $query->join('node__field_tp_oplachennaya_summa', 'o', 'o.entity_id = s.entity_id');
    $query->join('node__field_tp_stroki', 'str', 'str.field_tp_stroki_target_id = s.entity_id');
    $query->leftJoin('node__field_tp_utverzhdeno', 'paltezh_utverzhden', 'paltezh_utverzhden.entity_id = str.entity_id');
    $query->condition('paltezh_utverzhden.field_tp_utverzhdeno_value', 1);
    $query->addExpression('SUM(o.field_tp_oplachennaya_summa_value)');
    $sum = $query->execute()->fetchField();

    $schet_itogo = $schet->get('field_tp_itogo')->getString();
    $config = \Drupal::service('config.factory')->getEditable('trinion_tp.settings');
    if ($sum == 0)
      $sostoyanie_oplati_term = Term::load($config->get('schet_klienta_nov_tid'));
    elseif ($sum < $schet_itogo)
      $sostoyanie_oplati_term = Term::load($config->get('schet_klienta_chast_oplachen_tid'));
    else
      $sostoyanie_oplati_term = Term::load($config->get('schet_klienta_oplachen_tid'));
    return [
      'sostoyanie_oplati' => $sostoyanie_oplati_term->label(),
      'sostoyanie_oplati_tid' => $sostoyanie_oplati_term->id(),
      'summa_oplati' => $sum,
    ];
  }

  public function calculateSchetProdavtsa($schet) {
    $query = \Drupal::database()->select('node__field_tp_schet', 's')
      ->condition('s.bundle', 'stroka_scheta_prodavca_k_oplate')
      ->condition('s.field_tp_schet_target_id', $schet->id());
    $query->join('node__field_tp_roditelskiy_dokument', 'p', 'p.entity_id = s.entity_id');
    $query->join('node__field_tp_utverzhdeno', 'u', 'u.entity_id = p.field_tp_roditelskiy_dokument_target_id');
    $query->condition('u.field_tp_utverzhdeno_value', 1);
    $query->join('node__field_tp_oplachennaya_summa', 'o', 'o.entity_id = s.entity_id');
    $query->addExpression('SUM(o.field_tp_oplachennaya_summa_value)');
    $sum = $query->execute()->fetchField();

    $schet_itogo = $schet->get('field_tp_itogo')->getString();
    $config = \Drupal::service('config.factory')->getEditable('trinion_tp.settings');
    if ($sum == 0)
      $sostoyanie_oplati_term = Term::load($config->get('schet_klienta_nov_tid'));
    elseif ($sum < $schet_itogo)
      $sostoyanie_oplati_term = Term::load($config->get('schet_klienta_chast_oplachen_tid'));
    else
      $sostoyanie_oplati_term = Term::load($config->get('schet_klienta_oplachen_tid'));

    return [
      'sostoyanie_oplati' => $sostoyanie_oplati_term->label(),
      'sostoyanie_oplati_tid' => $sostoyanie_oplati_term->id(),
      'summa_oplati' => $sum,
    ];
  }

  /**
   * Получение или создание ноды Остаток товара
   * @param $artikul
   */
  public function getOstatokTovara($tovar_nid, $sklad_tid, $partiya_nid, $harakteristika_tid, $edinica_izereniya_tid, $create = FALSE) {
    $db = $this->connection;
    $query = $db->select('trinion_tp_ostatki', 'o')
      ->condition('o.tovar', $tovar_nid)
      ->condition('o.	sklad', $sklad_tid);
    if ($harakteristika_tid) {
      $query->condition('o.kharakteristika_tovara', $harakteristika_tid);
    }
    if ($edinica_izereniya_tid) {
      $query->condition('o.edinica_izmereniya', $edinica_izereniya_tid);
    }
    $query->fields('o');
    $res = $query->execute()->fetchAssoc();

    if (!$res) {
      if (!$create)
        return FALSE;
      $data = [
        'kolichestvo' => 0,
        'sklad' => $sklad_tid,
        'tovar' => $tovar_nid,
      ];
      if ($harakteristika_tid) {
        $data['kharakteristika_tovara'] = $harakteristika_tid;
      }
      if ($edinica_izereniya_tid) {
        $data['edinica_izmereniya'] = $edinica_izereniya_tid;
      }
      $query = $db->insert('trinion_tp_ostatki');
      $query->fields($data);
      $query->execute();
      $data['id'] = $db->lastInsertId();
    }
    else
      $data = $res;
    return $data;
  }

  public function updateOstatokTovara($id, $kolichestvo) {
    $query = $this->connection->update('trinion_tp_ostatki');
    $query->fields(['kolichestvo' => $kolichestvo]);
    $query->condition('id', $id);
    $query->execute();
  }

  public function updateRezervTovara($id, $kolichestvo) {
    $query = $this->connection->update('trinion_tp_rezervi');
    $query->fields(['kolichestvo' => $kolichestvo]);
    $query->condition('id', $id);
    $query->execute();
  }

  /**
   * Получение или создание ноды Остаток материала в производстве
   * @param $artikul
   */
  public function getOstatokVProizvodstveTovaraNode($tovar_nid, $podrazdelenie_tid, $harakteristika_tid, $edinica_izereniya_tid, $create = FALSE) {
    $query = \Drupal::entityQuery('node')
      ->condition('type', 'ostatki_materilov_v_proizvodstve')
      ->condition('field_tp_tovar', $tovar_nid)
      ->condition('field_tp_podrazdelenie', $podrazdelenie_tid);
    if ($harakteristika_tid) {
      $query->condition('field_tp_kharakteristika_tovara', $harakteristika_tid);
    }
    if ($edinica_izereniya_tid) {
      $query->condition('field_tp_edinica_izmereniya', $edinica_izereniya_tid);
    }
    $res = $query->accessCheck()->execute();
    if (empty($res)) {
      if (!$create)
        return FALSE;
      $tovar = Node::load($tovar_nid);
      $title = $tovar->label();

      $data = [
        'type' => 'ostatki_materilov_v_proizvodstve',
        'title' => $title,
        'uid' => \Drupal::currentUser()->id(),
        'status' => 1,
        'field_tp_kolichestvo' => 0,
        'field_tp_podrazdelenie' => $podrazdelenie_tid,
        'field_tp_tovar' => $tovar_nid,
      ];
      if ($harakteristika_tid) {
        $data['field_tp_kharakteristika_tovara'] = $harakteristika_tid;
      }
      if ($edinica_izereniya_tid) {
        $data['field_tp_edinica_izmereniya'] = $edinica_izereniya_tid;
      }
      $node = Node::create($data);
      $node->save();
    }
    else
      $node = Node::load(reset($res));
    return $node;
  }

  /**
   * Получеие живого остатка товара на складе
   * @param $tovar_nid
   * @param $sklad_tid
   * @param $partiya_nid
   * @param $harakteristika_tid
   * @param $edinica_izmereniya_tid
   */
  public function getLiveOstatok($tovar_nid, $sklad_tid, $harakteristika_tid, $edinica_izmereniya_tid) {
//    $query = \Drupal::entityQuery('node')
//      ->condition('type', 'ostatok')
//      ->condition('field_tp_tovar', $tovar_nid)
//      ->accessCheck(FALSE);
//    if ($edinica_izmereniya_tid)
//      $query->condition('field_tp_edinica_izmereniya', $edinica_izmereniya_tid);
//    if ($harakteristika_tid)
//      $query->condition('field_tp_kharakteristika_tovara', $harakteristika_tid);
//    if ($sklad_tid)
//      $query->condition('field_tp_sklad', $sklad_tid);
//    $res = $query->execute();

    $query = $this->connection->select('trinion_tp_ostatki', 'o')
      ->condition('o.tovar', $tovar_nid);
    $query->fields('o');
    if ($edinica_izmereniya_tid)
      $query->condition('o.edinica_izmereniya', $edinica_izmereniya_tid);
    if ($harakteristika_tid)
      $query->condition('o.kharakteristika_tovara', $harakteristika_tid);
    if ($sklad_tid)
      $query->condition('o.sklad', $sklad_tid);
    $ostatok = $query->execute()->fetchAssoc();

    if (empty($ostatok)) {
      return 0;
    }
//    $ostatok_node = Node::load(reset($res));
    $all_ostatok = $ostatok['kolichestvo'];

//    $query = \Drupal::entityQuery('node')
//      ->condition('type', 'rezerv')
//      ->condition('field_tp_tovar', $tovar_nid)
//      ->accessCheck(FALSE);
//    if ($edinica_izmereniya_tid)
//      $query->condition('field_tp_edinica_izmereniya', $edinica_izmereniya_tid);
//    if ($harakteristika_tid)
//      $query->condition('field_tp_kharakteristika_tovara', $harakteristika_tid);
//    if ($sklad_tid)
//      $query->condition('field_tp_sklad', $sklad_tid);
//    $res = $query->execute();

    $query = $this->connection->select('trinion_tp_rezervi', 'r')
      ->condition('r.tovar', $tovar_nid);
    $query->fields('r');
    if ($edinica_izmereniya_tid)
      $query->condition('r.edinica_izmereniya', $edinica_izmereniya_tid);
    if ($harakteristika_tid)
      $query->condition('r.kharakteristika_tovara', $harakteristika_tid);
    if ($sklad_tid)
      $query->condition('r.sklad', $sklad_tid);
    $res = $query->execute();
    $rezerv = 0;
    foreach ($res as $record) {
      $rezerv += $record->kolichestvo;
    }
    return $all_ostatok - $rezerv;
  }

  /**
   * Сохранение движения остатка товара
   * @param $ostatok
   * @param $tovar_kolichestvo
   * @param $document_nid
   * @param $tip
   * @throws \Drupal\Core\Entity\EntityStorageException
   * @throws \Drupal\Core\TypedData\Exception\MissingDataException
   */
  public function createDvigenieOstatokTovara($ostatok, $tovar_kolichestvo, $document_nid, $edinica_izereniya_tid, $tip) {
    if ($tip == '0')
      $tovar_kolichestvo *= -1;
    $data = [
      'ostatok' => $ostatok,
      'kolichestvo' => $tovar_kolichestvo,
      'document' => $document_nid,
      'tip_dvizheniya' => $tip,
    ];
    $query = $this->connection->insert('trinion_tp_dvizheniya');
    $query->fields($data);
    $query->execute();
  }

  /**
   * Сохранение резерва товара
   */
  public function createRezervTovara(Node $tovar, $sklad, $harakteristika_tid, $edinica_izereniya_tid, $tovar_kolichestvo, $zakaz_nid) {
    $data = [
      'kolichestvo' => $tovar_kolichestvo,
      'sklad' => $sklad,
      'tovar' => $tovar->id(),
      'document' => $zakaz_nid,
    ];
    if ($harakteristika_tid) {
      $data['kharakteristika_tovara'] = $harakteristika_tid;
    }
    if ($edinica_izereniya_tid) {
      $data['edinica_izmereniya'] = $edinica_izereniya_tid;
    }
    $query = $this->connection->insert('trinion_tp_rezervi');
    $query->fields($data);
    $query->execute();
  }

  /**
   * Получение ноды резерва по заказу
   * @param Node $zakaz_klienta
   * @return \Drupal\Core\Entity\EntityBase|\Drupal\Core\Entity\EntityInterface|false|null
   */
  public function getRezervTovaraByZakaz(Node $zakaz_klienta, $tovar_id, $sklad_tid, $harakteristika_tid, $edinica_izereniya_tid) {
    $query = $this->connection->select('trinion_tp_rezervi', 'r')
      ->condition('r.tovar', $tovar_id)
      ->condition('r.sklad', $sklad_tid)
      ->condition('r.document', $zakaz_klienta->id());
    if ($harakteristika_tid)
      $query->condition('r.kharakteristika_tovara', $harakteristika_tid);
    if ($edinica_izereniya_tid)
      $query->condition('r.edinica_izmereniya', $edinica_izereniya_tid);
    $query->fields('r');
    $res = $query->execute()->fetchAssoc();
    return $res ? $res : FALSE;
  }

  /**
   * Получение характеристик товара
   * @param Node $tovar
   * @return array
   */
  public function getHarakteristikiTovara(Node $tovar, $load_entity = FALSE, $price_based = TRUE) {
    $harakteristiki = [];
    if ($price_based) {
      $query = \Drupal::entityQuery('node')
        ->condition('type', 'cena')
        ->condition('field_tp_tovar', $tovar->id())
        ->condition('field_tp_tip_ceny', \Drupal::config('trinion_tp.settings')->get('roznichnaya_cena_tid'));
      $nids_ceni = $query->accessCheck()->execute();
      foreach ($nids_ceni as $cena_nid) {
        $cena = Node::load($cena_nid);
        foreach ($cena->get('field_tp_kharakteristika_tovara') as $harakteristika) {
          if ($harakteristika->entity) {
            if ($load_entity)
              $harakteristiki[$harakteristika->entity->id()] = $harakteristika->entity;
            else
              $harakteristiki[$harakteristika->entity->id()] = $harakteristika->entity->label();
          }
        }
      }
    }
    else {
      foreach ($tovar->get('field_tp_kharakteristika_tovara') as $char) {
        $char_id = $char->getString();
        $harakteristika = Term::load($char_id);
        if ($harakteristika) {
          if ($load_entity)
            $harakteristiki[$char_id] = $harakteristika;
          else
            $harakteristiki[$char_id] = $harakteristika->label();
        }
      }
    }
    return $harakteristiki;
  }

  /**
   * Получение розничной цены товара
   * @param Node $tovar
   * @return int
   */
  public function getRoznichnayaCenaTovara(Node $tovar, $harakteristika = NULL, $return_entity = FALSE) {
    $query = \Drupal::entityQuery('node')
      ->condition('type', 'cena')
      ->condition('field_tp_tovar', $tovar->id())
      ->condition('field_tp_tip_ceny', \Drupal::config('trinion_tp.settings')->get('roznichnaya_cena_tid'));
    if (!is_null($harakteristika))
      $query->condition('field_tp_kharakteristika_tovara', $harakteristika);
    $nids_ceni = $query->accessCheck()->execute();
    if ($nids_ceni) {
      $cena = Node::load(reset($nids_ceni));
      if ($cena)
        return $return_entity ? $cena : $cena->get('field_tp_cena')->getString();
    }
    return NULL;
  }

  /**
   * Получение пользовательской цены товара
   * @param Node $tovar
   * @return int
   */
  public function getPolzovatelskayaCenaTovara(Node $tovar, $harakteristika = NULL, $return_entity = FALSE) {
    $uid = \Drupal::currentUser()->id();
    if ($uid) {
      $user = User::load($uid);
      if ($uid) {
        $cena_tid = $user->get('field_tp_tip_ceny')->getString();
      }
    }
    if (empty($cena_tid)) {
      $config = \Drupal::service('config.factory')->getEditable('trinion_cart.settings');
      $cena_tid = $config->get('cena_dlia_anonima');
    }
    $query = \Drupal::entityQuery('node')
      ->condition('type', 'cena')
      ->condition('field_tp_tovar', $tovar->id())
      ->condition('field_tp_tip_ceny', $cena_tid);
    if (!empty($harakteristika))
      $query->condition('field_tp_kharakteristika_tovara', $harakteristika);
    $nids_ceni = $query->accessCheck()->execute();
    if ($nids_ceni) {
      $cena = Node::load(reset($nids_ceni));
      if ($cena)
        return $return_entity ? $cena : $cena->get('field_tp_cena')->getString();
    }
    return 0;
  }

  /**
   * Получение старой цены товара
   * @param Node $tovar
   * @return int
   */
  public function getStarayaCenaTovara(Node $tovar, $harakteristika = NULL, $return_entity = FALSE) {
    $config = \Drupal::service('config.factory')->getEditable('trinion_tp.settings');
    $cena_tid = $config->get('staraya_cena_tid');
    $query = \Drupal::entityQuery('node')
      ->condition('type', 'cena')
      ->condition('field_tp_tovar', $tovar->id())
      ->condition('field_tp_tip_ceny', $cena_tid);
    if (!empty($harakteristika))
      $query->condition('field_tp_kharakteristika_tovara', $harakteristika);
    $nids_ceni = $query->accessCheck()->execute();
    if ($nids_ceni) {
      $cena = Node::load(reset($nids_ceni));
      if ($cena)
        return $return_entity ? $cena : $cena->get('field_tp_cena')->getString();
    }
    return 0;
  }

  /**
   * Получение списка типов материалов - товаров
   */
  public function getProductBundles() {
    $bundles = \Drupal::config('trinion_tp.settings')->get('product_bundles');
    if (!$bundles)
      $bundles = [];
    if (!in_array('tovar', $bundles))
      $bundles[] = 'tovar';
    return $bundles;
  }

  /**
   * Поиск полученного платежа по Номеру входящего документа и Плательщику
   * @param $num_vh_doc
   * @param $platelshik
   * @return void
   */
  public function getPoluchenniyPlatezhByNumVhDocAndPlatelshik($num_vh_doc, $platelshik) {
    $query = \Drupal::entityQuery('node')
      ->condition('type', 'poluchennyy_platezh')
      ->condition('field_tp_nomer_vkh_plat', $num_vh_doc)
      ->condition('field_tp_platelschik', $platelshik);
    return $query->accessCheck()->execute();
  }

  /**
   * Получение дерева статей для поля
   */
  public function getDerevoStateyZatratDliaPoliya() {
    $statiya_zatrat = [];
    $parents = [];
    $terms = \Drupal::entityTypeManager()->getStorage('taxonomy_term')->loadTree('tp_charts_of_accounts', 0, 2);
    foreach ($terms as $item) {
      if ($item->parents[0] == 0) {
        $parents[$item->tid] = $item->name;
        $statiya_zatrat[$item->tid] = $item->name;
      }
      else {
        $parent_name = $parents[$item->parents[0]];
        unset($statiya_zatrat[$item->parents[0]]);
        $statiya_zatrat[$parent_name][$item->tid] = $item->name;
      }
    }
    return $statiya_zatrat;
  }

  /**
   * Получение спецификации товара
   * @param $tovar_nid
   * @param $harakteristika_tid
   * @return void
   */
  public function getSpecificatsiyaTovara($tovar_nid, $harakteristika_tid, $load_entity = FALSE) {
    $query = \Drupal::entityQuery('node')
      ->condition('type', 'mrp_specifikaciya')
      ->condition('field_tp_tovar', $tovar_nid)
      ->condition('field_mrp_activnay_specificatiya', 1);
    if ($harakteristika_tid)
      $query->condition('field_tp_kharakteristika_tovara', $harakteristika_tid);
    $res = $query->accessCheck()->execute();
    return $res ? Node::load(reset($res)) : FALSE;
  }

  public function movementName($name) {
    $names = [
      '+' => t('Entrance'),
      '-' => t('Write-downs'),
      '0' => t('Cancellation of document'),
      '1' => t('Inventory adjustment'),
    ];
    return $names[$name] ?? '';
  }
}
