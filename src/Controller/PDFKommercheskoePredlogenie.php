<?php

namespace Drupal\trinion_tp\Controller;

use Dompdf\Dompdf;
use Drupal\Core\Controller\ControllerBase;
use Drupal\node\Entity\Node;

/**
 * PDF Коммерческое предложение
 */
class PDFKommercheskoePredlogenie extends ControllerBase {

  /**
   * Builds the response.
   */
  public function build(Node $node) {
    $build['content'] = [
      '#theme' => 'kommercheskoe_predlogenie_pdf',
      "#node" => $node,
      "#root_path" => \Drupal::service('file_system')->realpath(''),
    ];
    $html = \Drupal::service('renderer')->render($build);
    $dompdf = new Dompdf();
    $dompdf->loadHtml($html);
    $options = $dompdf->getOptions();
    $options->set('chroot', DRUPAL_ROOT);
    $dompdf->setOptions($options);

    $dompdf->render();

    $dompdf->stream("kommercheskoe-predlogenie.pdf", ["Attachment" => false]);
    return ['#cache' => ['max-age' => 0]];
  }
}

