<?php

namespace Drupal\trinion_tp\Controller;

use Drupal\Core\Controller\ControllerBase;
use Drupal\Core\Url;
use Drupal\node\Entity\NodeType;

/**
 * Select product type before creation product
 */
class SozdanieTovara extends ControllerBase {

  /**
   * Builds the response.
   */
  public function build() {
    $types = \Drupal::service('trinion_tp.helper')->getProductBundles();
    $content = [];
    foreach ($types as $type_id) {
      if ($type_id) {
        $type = NodeType::load($type_id);
        $content[] = [
          '#prefix' => '<div>',
          '#sufix' => '</div>',
          '#type' => 'link',
          '#title' => $type->label(),
          '#url' => Url::fromRoute('node.add', ['node_type' => $type_id]),
        ];
      }
    }
    return $content;
  }
}

