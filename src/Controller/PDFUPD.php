<?php

namespace Drupal\trinion_tp\Controller;

use Dompdf\Dompdf;
use Drupal\Core\Controller\ControllerBase;
use Drupal\node\Entity\Node;

/**
 * PDF УПД
 */
class PDFUPD extends ControllerBase {

  /**
   * Builds the response.
   */
  public function build(Node $node) {
    if ($org = $node->get('field_tp_organizaciya')->first()) {
      $org = $org->entity;
    }
    if ($customer = $node->get('field_tp_otgruzka_dlya')->first()) {
      $customer = $customer->entity;
    }
    if ($zakaz = $node->get('field_tp_zakaz_klienta')->first()) {
      $zakaz = $zakaz->entity;
    }
    $items = [];
    foreach ($node->get('field_tp_stroki') as $item) {
      $items[] = Node::load($item->entity->id());
    }
    $build['content'] = [
      '#theme' => 'upd_pdf',
      "#customer" => $customer,
      "#org" => $org,
      "#zakaz" => $zakaz,
      "#otgruzka" => $node,
      "#date" => \Drupal::service('trinion_tp.helper')->getPluralDate($node->get('field_tp_data')->getString()),
      "#zakaz_date" => \Drupal::service('trinion_tp.helper')->getPluralDate($zakaz->get('field_tp_data')->getString()),
      "#items" => $items
    ];
    $html = \Drupal::service('renderer')->render($build);

    $dompdf = new Dompdf();
    $dompdf->loadHtml($html);
    $dompdf->setPaper('A4', 'landscape');
    $font = $dompdf->getFontMetrics()->getFont("Arial");
    $dompdf->getCanvas()->page_text(16, 300, "Page: {PAGE_NUM} of {PAGE_COUNT}", $font, 8);
    $options = $dompdf->getOptions();
    $options->set('isPhpEnabled', TRUE);
    $options->set('chroot', DRUPAL_ROOT);
    $dompdf->setOptions($options);
    $dompdf->render();

    $x          = 750;
    $y          = 570;
    $text       = "Страница {PAGE_NUM} из {PAGE_COUNT}";
    $font       = $dompdf->getFontMetrics()->get_font('Arial', 'normal');
    $size       = 7;
    $color      = array(0,0,0);
    $word_space = 0.0;
    $char_space = 0.0;
    $angle      = 0.0;

    $dompdf->getCanvas()->page_text(
      $x, $y, $text, $font, $size, $color, $word_space, $char_space, $angle
    );
    $dompdf->stream("upd.pdf", ["Attachment" => false]);
    return ['#cache' => ['max-age' => 0]];
  }

}

