<?php

namespace Drupal\trinion_tp\Controller;

use Drupal\Core\Controller\ControllerBase;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\node\Entity\Node;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Загрузка оплат из Frontol
 */
class FrontolZagruzkaOplat {

  /**
   * Создание файла-флага для получения оплат
   */
  public static function createFlagForImportPaymentsLocalPath() {
    $file_path = DRUPAL_ROOT . '/' .\Drupal::config('trinion_tp.settings')->get('frontol_get_payment_flag');
    $str = '$$$TRANSACTIONSBYDATERANGE
' . date('d.m.Y', strtotime('-4 day')) . '; ' . date('d.m.Y');
    $fp = fopen($file_path, 'w');
    fwrite($fp, $str);
    fclose($fp);
  }

  /**
   * Считывание файлов с выгруженными из фронтола оплатами и создание Приемов денег от клиента
   * @throws \Drupal\Core\Entity\EntityStorageException
   */
  public static function createPayments() {
    $csv = self::readLocalPaymentsFile();
    foreach ($csv as $data) {
      if (isset($data[3]) && $data[3] == 55 && $zakaz_number = $data[18]) {
        $num_vh_doc = $data[0] . ';' . $data[1] . ';' . $data[2];
        $zakaz_number = intval($zakaz_number);
        $query = \Drupal::entityQuery('node')
          ->condition('type', 'zakaz_klienta')
          ->condition('title', $zakaz_number);
        $res = $query->accessCheck()->execute();
        $zakaz = NULL;
        if ($res) {
          $nid = reset($res);
          $zakaz = Node::load($nid);
        }
        if (is_null($zakaz))
          continue;
        $exists = \Drupal::service('trinion_tp.helper')->getPoluchenniyPlatezhByNumVhDocAndPlatelshik($num_vh_doc, $zakaz->get('field_tp_zakaz_dlya')->getString());
        if ($exists)
          continue;

        $schet_controller = new SozdanieSchetaKlienta();
        $schet = $schet_controller->createSchet($zakaz);

        $scheta = [];
        $total_payed = str_replace(',', '.',$data[19]);
        $bundle = $zakaz->bundle();

        if ($bundle == 'zakaz_klienta') {
          $scheta[] = [
            'schet' => $schet,
            'summa' => $total_payed,
          ];
        }

        $sistema_oplati = FALSE;
        foreach ($csv as $data2) {
          if (isset($data2[5]) && $data2[5] == $data[5] && $data2[3] == 40) {
            if ($data2[9] == 0)
              $sistema_oplati = \Drupal::config('trinion_tp.settings')->get('nalichnie_tid');
            else
              $sistema_oplati = \Drupal::config('trinion_tp.settings')->get('oplata_po_karte_tid');
          }
        }

        $platezh_controller = new SozdaniePoluchennogoPlatezha();
        $platezh_controller->createPoluchenniyPlatezh($scheta, $num_vh_doc, $sistema_oplati, 'Frontol. ' . iconv('Windows-1251', 'UTF-8', implode(';', $data)));

        $sklad_tid = $zakaz->get('field_tp_stroki')->first()->entity->get('field_tp_sklad')->getString();
        $otgruzka_controller = new SozdanieOtgruzki();
        $otgruzka = $otgruzka_controller->createOtgruzka($zakaz, $sklad_tid);

        $utverzhdenie_cotroller = new UtverzhdenieDokumenta();
        $utverzhdenie_cotroller->build($otgruzka, 1);
      }
    }
    $file_path = DRUPAL_ROOT . '/' .\Drupal::config('trinion_tp.settings')->get('frontol_get_payment_file');
    unlink($file_path);
  }

  /**
   * @return void
   */
  public static function readLocalPaymentsFile() {
    $file_path = \Drupal::config('trinion_tp.settings')->get('frontol_get_payment_file');
    $csv = [];
    if (file_exists($file_path)) {
      if (($handle = fopen($file_path, "r")) !== FALSE) {
        $csv = [];
        while (($data = fgetcsv($handle, 10000, ";")) !== FALSE) {
          $csv[] = $data;
        }
        fclose($handle);
      }
    }
    return $csv;
  }
}
