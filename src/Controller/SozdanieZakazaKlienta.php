<?php

namespace Drupal\trinion_tp\Controller;

use Dompdf\Dompdf;
use Drupal\Core\Ajax\AjaxResponse;
use Drupal\Core\Ajax\RedirectCommand;
use Drupal\Core\Controller\ControllerBase;
use Drupal\node\Entity\Node;

/**
 * Создание Заказа покупателя
 */
class SozdanieZakazaKlienta extends ControllerBase {

  /**
   * Builds the response.
   */
  public function build(Node $node) {
    $uid = \Drupal::currentUser()->id();
    $zakaz_data = [
      'type' => 'zakaz_klienta',
      'title' => $this->getNomer(),
      'uid' => $uid,
      'status' => 1,
      'field_tp_komm_predlozhenie' => ['target_id' => $node->id()],
    ];
    foreach ($node->getFields() as $field_name => $f) {
      if (strpos($field_name, 'field_') === 0) {
        if ($field_name == 'field_tp_schet_dlya') {
          $zakaz_data['field_tp_zakaz_dlya'] = $node->get($field_name)->getValue();
        }
        elseif ($field_name == 'field_tp_stroki') {
          foreach ($node->get($field_name) as $stroka_uit) {
            /** @var Node $new_stroka */
            $new_stroka = clone $stroka_uit->entity->createDuplicate();
            $new_stroka->created = time();
            $new_stroka->uid = $uid;
            $new_stroka->save();
            $zakaz_data[$field_name][] = ['target_id' => $new_stroka->id()];
          }
        }
        else
          $zakaz_data[$field_name] = $node->get($field_name)->getValue();
      }
    }
    $zakaz_data['field_tp_utverzhdeno'] = 0;
    $schet = Node::create($zakaz_data);
    $schet->save();
    $response = new AjaxResponse();
    $response->addCommand(new RedirectCommand('/node/' . $schet->id()));

    return $response;
  }

  public function getNomer() {
    return \Drupal::service('trinion_tp.helper')->getNextDocumentNumber('zakaz_klienta');
  }
}

