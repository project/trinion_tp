<?php

namespace Drupal\trinion_tp\Controller;

use Dompdf\Dompdf;
use Drupal\Core\Ajax\AjaxResponse;
use Drupal\Core\Ajax\RedirectCommand;
use Drupal\Core\Controller\ControllerBase;
use Drupal\node\Entity\Node;

/**
 * Создание Заказа поставщику
 */
class SozdanieZakazaPostavshiku extends ControllerBase {

  /**
   * Builds the response.
   */
  public function build(Node $node) {
//    if ($this->checkExistNomer($values['zakaz_num'])) {
//      \Drupal::messenger()->addWarning('Заказ клиента с таким номером уже есть в этом году');
//    }

    $uid = \Drupal::currentUser()->id();
    $schet_data = [
      'type' => 'zakaz_postavschiku',
      'title' => $this->getNomer(),
      'uid' => $uid,
      'status' => 1,
      'field_tp_zakaz_klienta' => ['target_id' => $node->id()],
    ];
    foreach ($node->getFields() as $field_name => $f) {
      if (strpos($field_name, 'field_') === 0) {
        if ($field_name == 'field_tp_stroki') {
          foreach ($node->get($field_name) as $stroka_uit) {
            /** @var Node $new_stroka */
            $new_stroka = clone $stroka_uit->entity->createDuplicate();
            $new_stroka->created = time();
            $new_stroka->uid = $uid;
            $new_stroka->save();
            $schet_data[$field_name][] = ['target_id' => $new_stroka->id()];
          }
        }
      }
      if ($field_name == 'field_tp_otvetstvennyy') {
        $schet_data[$field_name] = $node->get($field_name)->getValue();
      }
    }
    $schet_data['field_tp_utverzhdeno'] = 0;
    $schet = Node::create($schet_data);
    $schet->save();
    $response = new AjaxResponse();
    $response->addCommand(new RedirectCommand('/node/' . $schet->id()));

    return $response;
  }

  public function checkExistNomer($nomer) {
    $query = \Drupal::database()->select('node_field_data', 'n')
      ->condition('n.type', 'zakaz_postavschiku')
      ->condition('n.title', $nomer);
    $query->join('node__field_tp_data', 'd', 'd.entity_id = n.nid');
    $query->condition('d.field_tp_data_value', date('Y') . '-00-00', '>=');
    $query->condition('d.field_tp_data_value', date('Y') . '-12-31', '<=');
    $query->addField('n', 'title');
    return $query->countQuery()->execute()->fetchField();
  }

  public function getNomer() {
    return \Drupal::service('trinion_tp.helper')->getNextDocumentNumber('zakaz_postavschiku');
  }
}

