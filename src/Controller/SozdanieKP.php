<?php

namespace Drupal\trinion_tp\Controller;

use Dompdf\Dompdf;
use Drupal\Core\Ajax\AjaxResponse;
use Drupal\Core\Ajax\RedirectCommand;
use Drupal\Core\Controller\ControllerBase;
use Drupal\node\Entity\Node;

/**
 * Создание Коммерческого предложения
 */
class SozdanieKP extends ControllerBase {

  /**
   * Builds the response.
   */
  public function build(Node $node) {
//    if ($this->checkExistNomer($values['zakaz_num'])) {
//      \Drupal::messenger()->addWarning('Заказ клиента с таким номером уже есть в этом году');
//    }

    $uid = \Drupal::currentUser()->id();
    $kp_data = [
      'type' => 'kommercheskoe_predlozhenie',
      'title' => $this->getNomer(),
      'uid' => $uid,
      'status' => 1,
      'field_tp_sdelka' => ['target_id' => $node->id()],
    ];
    foreach ($node->getFields() as $field_name => $f) {
      if (strpos($field_name, 'field_') === 0) {
        if ($field_name == 'field_tl_kompaniya') {
          $kp_data['field_tp_schet_dlya'] = $node->get($field_name)->getValue();
        }
        if ($field_name == 'field_tl_otvetstvennyy') {
          $kp_data['field_tp_otvetstvennyy'] = $node->get($field_name)->getValue();
        }
        $kp_data[$field_name] = $node->get($field_name)->getValue();
      }
    }
    $kp_data['field_tp_utverzhdeno'] = 0;
    $schet = Node::create($kp_data);
    $schet->save();
    $response = new AjaxResponse();
    $response->addCommand(new RedirectCommand('/node/' . $schet->id()));

    return $response;
  }

  public function checkExistNomer($nomer) {
    $query = \Drupal::database()->select('node_field_data', 'n')
      ->condition('n.type', 'kommercheskoe_predlozhenie')
      ->condition('n.title', $nomer);
    $query->join('node__field_tp_data', 'd', 'd.entity_id = n.nid');
    $query->condition('d.field_tp_data_value', date('Y') . '-00-00', '>=');
    $query->condition('d.field_tp_data_value', date('Y') . '-12-31', '<=');
    $query->addField('n', 'title');
    return $query->countQuery()->execute()->fetchField();
  }

  public function getNomer() {
    return \Drupal::service('trinion_tp.helper')->getNextDocumentNumber('kommercheskoe_predlozhenie');
  }
}

