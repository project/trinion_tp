<?php
namespace Drupal\trinion_tp\Controller;


use Drupal\Component\Utility\Html;
use Drupal\node\Entity\Node;
use Drupal\taxonomy\Entity\Term;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;

class TovarAutocomplete {
  public function autocomplete(Request $request) {
    $referer = $request->server->get('HTTP_REFERER');
    $nds_default = 0;
    $skidka = 0;

    if (strpos($referer, '/node/add/')) {
      $parts = parse_url($referer);
      parse_str($parts['query'], $query);
      $bundle = explode('/', $parts['path']);
      $bundle = array_pop($bundle);
      if (isset($query['comp']) && preg_match('/\((\d+)\)("|)\s*$/', $query['comp'], $match)) {
        if ($node = Node::load($match[1])) {
          $skidka = floatval($node->get('field_tl_procent_skidki')->getString());
        }
      }
      if (isset($query['org']))
        $org_tid = $query['org'];
    }
    elseif (preg_match('/\/node\/(\d+)\/edit/', $referer, $match)) {
      $node = Node::load($match[1]);
      $bundle = $node->bundle();
      if ($node->hasField('field_tp_organizaciya'))
        $org_tid = $node->get('field_tp_organizaciya')->getString();
    }
    if (!empty($org_tid) && $org = Term::load($org_tid)) {
      $nds_default = $org->get('field_tl_nds')->getString();
    }
    if ($bundle == 'mrp_specifikaciya' || $bundle == 'tp_sales_plan')
      $price_based = FALSE;

    $string = $request->query->get('q');
    $matches = [];
    if ($string) {
      $query = \Drupal::entityQuery('node');
      $query->condition('type', \Drupal::service('trinion_tp.helper')->getProductBundles(), 'IN');
      $or = $query->orConditionGroup();
      $or->condition('field_tp_artikul', $string, 'CONTAINS');
      $or->condition('title', $string, 'CONTAINS');
      $query->condition($or);
      $query->range(0, 25);
      $nids = $query->accessCheck()->execute();
      if (\Drupal::config('trinion_tp.settings')->get('harakteristiki')) {
        foreach ($nids as $nid) {
          if ($tovar = Node::load($nid)) {
            $harakteristiki = \Drupal::service('trinion_tp.helper')->getHarakteristikiTovara($tovar, FALSE, $price_based);
            $cena = NULL;

            if (!$harakteristiki) {
              $cena = \Drupal::service('trinion_tp.helper')->getRoznichnayaCenaTovara($tovar);
              if (is_null($cena))
                $cena = '0';
            }
            $name = $tovar->label() . ' [' . $tovar->get('field_tp_artikul')->getString() . ']';
            $value = Html::escape($name);
            $label = Html::escape($name);
            $opisanie = $tovar->hasField('field_tp_opisanie_tovara') && ($opisanie = $tovar->get('field_tp_opisanie_tovara')->first()) ? $opisanie->getValue()['summary'] : '';
            if ($edinica_izmereniya = $tovar->get('field_tp_edinica_izmereniya')->first()) {
              $edinica_izmereniya_id = $edinica_izmereniya->entity->id();
              $edinica_izmereniya_name = $edinica_izmereniya->entity->label();
            }
            $matches[] = ['value' => $value, 'label' => $label, 'harakteristiki' => $harakteristiki, 'edinica_izmereniya_id' => $edinica_izmereniya_id, 'edinica_izmereniya_name' => $edinica_izmereniya_name, 'nds' => $nds_default, 'skidka' => $skidka, 'cena' => $cena, 'opisanie' => $opisanie];

          }
        }
      }
      else {
        foreach ($nids as $nid) {
          if ($tovar = Node::load($nid)) {
            $cena = \Drupal::service('trinion_tp.helper')->getRoznichnayaCenaTovara($tovar);

            if (is_null($cena))
              $cena = '0';
            $name = $tovar->label() . ' [' . $tovar->get('field_tp_artikul')->getString() . ']';
            $value = Html::escape($name);
            $label = Html::escape($name);
            $opisanie = '';
            if ($tovar->hasField('field_tp_opisanie_tovara')) {
              if ($val = $tovar->get('field_tp_opisanie_tovara')->first())
                $opisanie = $val->getValue()['summary'];
            }
            if ($edinica_izmereniya = $tovar->get('field_tp_edinica_izmereniya')->first()) {
              $edinica_izmereniya_id = $edinica_izmereniya->entity->id();
              $edinica_izmereniya_name = $edinica_izmereniya->entity->label();
            }
            $matches[] = ['value' => $value, 'label' => $label, 'cena' => $cena, 'edinica_izmereniya_id' => $edinica_izmereniya_id, 'edinica_izmereniya_name' => $edinica_izmereniya_name, 'nds' => $nds_default, 'skidka' => $skidka, 'opisanie' => $opisanie];

          }
        }
      }
    }
    return new JsonResponse($matches);
  }

  public function harakteristikaCena(Request $request) {
    $query = \Drupal::entityQuery('node')
      ->condition('type', 'cena')
      ->condition('field_tp_tip_ceny', \Drupal::config('trinion_tp.settings')->get('roznichnaya_cena_tid'))
      ->condition('field_tp_tovar', \Drupal::service('trinion_tp.helper')->tovarfromAutocomleteString($request->get('tovar'))->id())
      ->condition('field_tp_kharakteristika_tovara', $request->get('harakteristika'));
    $res = $query->accessCheck()->execute();
    if ($res) {
      $cena = Node::load(reset($res));
      $cena = $cena->get('field_tp_cena')->getString();
    }
    else
      $cena = 0;
    return new JsonResponse(['cena' => $cena]);
  }

  public function harakteristikaOstatok(Request $request) {
    $tovar = \Drupal::service('trinion_tp.helper')->tovarfromAutocomleteString($request->get('tovar'));
    $stock = \Drupal::service('trinion_tp.helper')->getLiveOstatok($tovar->id(), $request->get('sklad'), $request->get('harakteristika'), $tovar->get('field_tp_edinica_izmereniya')->getString());

    return new JsonResponse(['ostatok' => $stock]);
  }
}
