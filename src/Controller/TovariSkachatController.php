<?php

namespace Drupal\trinion_tp\Controller;

use Drupal\Core\Controller\ControllerBase;
use Drupal\Core\Database\Connection;
use Drupal\Core\Routing\TrustedRedirectResponse;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Returns responses for Trinion tp routes.
 */
class TovariSkachatController extends ControllerBase {

  /**
   * The database connection.
   *
   * @var \Drupal\Core\Database\Connection
   */
  protected $connection;

  /**
   * The controller constructor.
   *
   * @param \Drupal\Core\Database\Connection $connection
   *   The database connection.
   */
  public function __construct(Connection $connection) {
    $this->connection = $connection;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('database')
    );
  }

  /**
   * Builds the response.
   */
  public function build() {
    $tp_settings = \Drupal::config('trinion_tp.settings');
    $tovar_bundles = $tp_settings->get('product_bundles');
    if (empty($tovar_bundles)) {
      \Drupal::messenger()->addError('Не выбран ни один тип материала в качестве сущности товаров.');
      $redirect = new TrustedRedirectResponse('/tovari');
      $redirect->send();
      exit;
    }
    if ($tovar_bundles) {
      $cols = [];
      $query = $this->connection->select('node_field_data', 'n');
      $query->condition('n.type', $tovar_bundles, 'IN');
      $query->condition('n.status', 1);
      $query->addField('n', 'title');
      $cols[] = 'Наименование';
      $query->join('node__field_tp_artikul', 'artikul', 'artikul.entity_id = n.nid AND artikul.bundle = n.type');
      $query->addField('artikul', 'field_tp_artikul_value');
      $cols[] = 'Артикул';
      $query->leftJoin('node__field_tp_tovar', 'cena_tovar', 'cena_tovar.field_tp_tovar_target_id = n.nid');
      $query->condition('cena_tovar.bundle', 'cena');
      $query->leftJoin('node__field_tp_tip_ceny', 'tip_ceni', 'tip_ceni.entity_id = cena_tovar.entity_id');
      $query->condition('tip_ceni.bundle', 'cena');
      $query->condition('tip_ceni.field_tp_tip_ceny_target_id', $tp_settings->get('roznichnaya_cena_tid'));
      $query->leftJoin('node__field_tp_cena', 'cena', 'cena.entity_id = cena_tovar.entity_id');
      $query->condition('cena.bundle', 'cena');
      $cols[] = 'Цена';
      $query->addField('cena', 'field_tp_cena_value');
      $query->orderBy('n.created', 'DESC');
      $res = $query->execute();

      header( 'Content-Type: text/csv' );
      header( 'Content-Disposition: attachment;filename=tovari_s_cenami.csv');
      $out = fopen('php://output', 'w');
//      $rows = [];
      fputcsv($out, $cols);
      foreach ($res as $record) {
        fputcsv($out, (array)$record);
//        $rows[] = $record;
      }
      fclose($out);
      exit;
    }
  }

}
