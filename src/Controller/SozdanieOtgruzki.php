<?php

namespace Drupal\trinion_tp\Controller;

use Drupal\Core\Ajax\AjaxResponse;
use Drupal\Core\Ajax\RedirectCommand;
use Drupal\Core\Controller\ControllerBase;
use Drupal\node\Entity\Node;

/**
 * Создание Отгрузки
 */
class SozdanieOtgruzki extends ControllerBase {

  /**
   * Builds the response.
   */
  public function build(Node $node) {
    $otgruzka = $this->createOtgruzka($node);
    $response = new AjaxResponse();
    if (!$otgruzka) {
      \Drupal::messenger()->addStatus('Все товары из заказа уже отгружены');
      $response->addCommand(new RedirectCommand('/node/' . $node->id()));
    }
    else {
      $response->addCommand(new RedirectCommand('/node/' . $otgruzka->id()));
    }
    return $response;
  }

  /**
   * Создание отгрузки на основе заказа
   * @param Node $node
   * @return \Drupal\Core\Entity\ContentEntityBase|\Drupal\Core\Entity\EntityBase|\Drupal\Core\Entity\EntityInterface|Node|false
   * @throws \Drupal\Core\Entity\EntityStorageException
   */
  public function createOtgruzka(Node $node, $sklad_tid = NULL) {
    $otgruzki = \Drupal::service('trinion_tp.helper')->otgruzkiByZakazKlienta($node->id());
    if ($otgruzki) {
      $ne_otgruzheno = \Drupal::service('trinion_tp.helper')->neOtgruzhenoIzZakazKlienta($node, $otgruzki);
    }
    $uid = \Drupal::currentUser()->id();
    $otgruzka_data = [
      'type' => 'otgruzka',
      'title' => $this->getNomer(),
      'uid' => $uid,
      'status' => 1,
      'field_tp_zakaz_klienta' => ['target_id' => $node->id()],
    ];
    if ($sklad_tid)
      $otgruzka_data['field_tp_sklad'] = ['target_id' => $sklad_tid];

    foreach ($node->getFields() as $field_name => $f) {
      if (strpos($field_name, 'field_') === 0) {
        if ($field_name == 'field_tp_zakaz_dlya') {
          $otgruzka_data['field_tp_otgruzka_dlya'] = $node->get($field_name)->getValue();
        }
        elseif ($field_name == 'field_tp_utverzhdeno') {
          continue;
        }
        elseif ($field_name == 'field_tp_stroki') {
          foreach ($node->get($field_name) as $stroka_uit) {
            $stroka_uit = $stroka_uit->entity;
            $tovar_nid = $stroka_uit->get('field_tp_tovar')->getString();
            $sklad_tid = $stroka_uit->get('field_tp_sklad')->getString();
            $edinica_izereniya_tid = $stroka_uit->get('field_tp_edinica_izmereniya')->getString();
            $harakteristika_tid = $stroka_uit->get('field_tp_kharakteristika_tovara')->getString();
            $key = "{$tovar_nid}-{$sklad_tid}-{$edinica_izereniya_tid}-{$harakteristika_tid}";

            if (!isset($ne_otgruzheno) || isset($ne_otgruzheno[$key])) {
              /** @var Node $new_stroka */
              $new_stroka = clone $stroka_uit->createDuplicate();
              $new_stroka->created = time();
              $new_stroka->uid = $uid;
              if (isset($ne_otgruzheno[$key])) {
                $new_stroka->field_tp_kolichestvo = $ne_otgruzheno[$key];
              }
              $new_stroka->save();
              $otgruzka_data[$field_name][] = ['target_id' => $new_stroka->id()];
            }
          }
        }
        else
          $otgruzka_data[$field_name] = $node->get($field_name)->getValue();
      }
    }
    $otgruzka = FALSE;
    if ($otgruzka_data['field_tp_stroki']) {
      $otgruzka = Node::create($otgruzka_data);
      $otgruzka->save();
    }
    return $otgruzka;
  }

  public function getNomer() {
    return \Drupal::service('trinion_tp.helper')->getNextDocumentNumber('otgruzka');
  }
}

