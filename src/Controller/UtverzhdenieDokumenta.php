<?php

namespace Drupal\trinion_tp\Controller;

use Dompdf\Dompdf;
use Drupal\Core\Ajax\AjaxResponse;
use Drupal\Core\Ajax\AlertCommand;
use Drupal\Core\Ajax\MessageCommand;
use Drupal\Core\Ajax\RedirectCommand;
use Drupal\Core\Controller\ControllerBase;
use Drupal\node\Entity\Node;

/**
 * Утверждение или отмена документа
 */
class UtverzhdenieDokumenta extends ControllerBase {

  /**
   * Builds the response.
   * @param $op 0 - аннулирован, 1 - зарезервирован (+утвержден), 2 - утвержден
   */
  public function build(Node $node, $op) {
    $response = new AjaxResponse();
    $bundle = $node->bundle();
    $error = FALSE;
    $transaction = \Drupal::database()->startTransaction();
    if ($bundle == 'postuplenie_tovarov') {
      $sklad = $node->get('field_tp_sklad')->getString();
      if (empty($sklad)) {
        $error = TRUE;
        \Drupal::messenger()->addWarning('Поле Склад не заполнено');
      }
    }
    elseif ($bundle == 'zakaz_klienta') {
      foreach ($node->get('field_tp_stroki') as $stroka) {
        $stroka = $stroka->entity;
        $sklad_tid = $stroka->get('field_tp_sklad')->getString();
        $tovar = $stroka->get('field_tp_tovar')->first()->entity;
        if (!$sklad_tid) {
          $error = TRUE;
          \Drupal::messenger()->addWarning('У товара ' . $tovar->label() . ' поле Склад не заполнено');
        }
        elseif ($op == 1) {
          $edinica_izmereniya_tid = $stroka->get('field_tp_edinica_izmereniya')->getString();
          $harakteristika_tid = $stroka->get('field_tp_kharakteristika_tovara')->getString();
          $jivoy_ostatok = \Drupal::service('trinion_tp.helper')->getLiveOstatok($tovar->id(), $sklad_tid, $harakteristika_tid, $edinica_izmereniya_tid);
          $kolichestvo = $stroka->get('field_tp_kolichestvo')->getString();
          if ($jivoy_ostatok < $kolichestvo) {
            $error = TRUE;
            \Drupal::messenger()->addWarning('У товара ' . $tovar->label() . ' живой остаток на складе ' . $stroka->get('field_tp_sklad')->first()->entity->label() . ' меньше заказа. На складе в живом остатке ' . $jivoy_ostatok);
          }
        }
      }
    }
    elseif ($bundle == 'otgruzka') {
      $sklad_tid = $node->get('field_tp_sklad')->getString();
      if (empty($sklad_tid)) {
        $error = TRUE;
        \Drupal::messenger()->addWarning('Поле Склад не заполнено');
      }
//      else {
//        foreach ($node->get('field_tp_stroki') as $stroka) {
//          $stroka = $stroka->entity;
//          $tovar = $stroka->get('field_tp_tovar')->first()->entity;
//          if ($op == 1) {
//            $edinica_izmereniya_tid = $stroka->get('field_tp_edinica_izmereniya')->getString();
//            $harakteristika_tid = $stroka->get('field_tp_kharakteristika_tovara')->getString();
//            $jivoy_ostatok = \Drupal::service('trinion_tp.helper')->getLiveOstatok($tovar->id(), $sklad_tid, $harakteristika_tid, $edinica_izmereniya_tid);
//            $kolichestvo = $stroka->get('field_tp_kolichestvo')->getString();
//            if ($jivoy_ostatok < $kolichestvo) {
//              $error = TRUE;
//              \Drupal::messenger()->addWarning('У товара ' . $tovar->label() . ' живой остаток на складе ' . $node->get('field_tp_sklad')->first()->entity->label() . ' меньше заказа. На складе в живом остатке ' . $jivoy_ostatok);
//            }
//          }
//        }
//      }
    }

    if (!$error) {
      if ($bundle == 'postuplenie_tovarov') {
        $node->field_tp_utverzhdeno = $op;
        $this->utverzhdeniePostupleniyaTovarov($node, $op);
        \Drupal::messenger()->addStatus('Остатки на складе ' . ($op ? 'увеличены' : 'уменьшены'));
      } elseif ($bundle == 'zakaz_klienta') {
        $this->utverzhdenieZakazaKlienta($node, $op);
        if ($op == 0) {
          $node->field_tp_utverzhdeno = 0;
          $node->field_tp_zarezervirovan = 0;
          \Drupal::messenger()->addStatus('Резерв товаров удален');
        }
        elseif ($op == 1) {
          $node->field_tp_utverzhdeno = 1;
          $node->field_tp_zarezervirovan = 1;
          \Drupal::messenger()->addStatus('Резерв товаров создан');
          if ($node->get('field_tp_zakaz_dlya')->first()->entity->get('field_tl_roznichnyy_pokupatel')->getString()) {
            $vigruzka = new FrontolVigruzkaZakazov();
            $vigruzka->createOrderFile($node);
            \Drupal::messenger()->addStatus('Заказ выгружен во фронтол, через несколько секунд он там появится.');
          }
        }
        elseif ($op == 2) {
          $node->field_tp_utverzhdeno = 1;
          \Drupal::messenger()->addStatus('Заказ утвержден');
        }
      }
      elseif ($bundle == 'otgruzka') {
        $node->field_tp_utverzhdeno = $op;
        $this->utverzhdenieOtgruzki($node, $op);
      }
      elseif ($bundle == 'kommercheskoe_predlozhenie') {
        $node->field_tp_utverzhdeno = $op;
      }
      elseif ($bundle == 'zakaz_postavschiku') {
        $node->field_tp_utverzhdeno = $op;
      }
      elseif ($bundle == 'mrp_zakaz_na_proizvodstvo') {
        $node->field_tp_utverzhdeno = $op;
      }
      elseif ($bundle == 'schet') {
        $node->field_tp_utverzhdeno = $op;
      }
      elseif ($bundle == 'mrp_zakaz_na_proizvodstvo') {
        $node->field_tp_utverzhdeno = $op;
      }
      elseif ($bundle == 'schet_postavschika') {
        $node->field_tp_utverzhdeno = $op;
      }
      elseif ($bundle == 'poluchennyy_platezh') {
        $node->field_tp_utverzhdeno = $op;
        $node->save();
        $this->utverzhdeniePoluchennogoPlatezha($node, $op);
      }
      elseif ($bundle == 'otpravlennyy_platezh') {
        $node->field_tp_utverzhdeno = $op;
        $node->save();
        $this->utverzhdenieOtpravlennogoPlatezha($node, $op);
      }
      elseif ($bundle == 'peremeshchenie_tovarov') {
        $this->utverzhdeniePeremesheniyaTovarov($node, $op);
        if ($op == 0) {
          $node->field_tp_utverzhdeno = 0;
          \Drupal::messenger()->addStatus(t('Adjustment is canceled'));
        }
        elseif ($op == 1) {
          $node->field_tp_utverzhdeno = 1;
          \Drupal::messenger()->addStatus(t('Adjustment is approved'));
        }
      }
      elseif ($bundle == 'trebovanie_nakladnaya') {
        $this->utverzhdenieTrebovaniyaNakladnoy($node, $op);
        if ($op == 0) {
          $node->field_tp_utverzhdeno = 0;
          \Drupal::messenger()->addStatus('Перемещение товаров аннулировано');
        }
        elseif ($op == 1) {
          $node->field_tp_utverzhdeno = 1;
          \Drupal::messenger()->addStatus('Перемещение товаров утверждено');
        }
      }
      elseif ($bundle == 'vypusk_produkcii') {
        $this->utverzhdenieVipuskaProdukcii($node, $op);
        if ($op == 0) {
          $node->field_tp_utverzhdeno = 0;
          \Drupal::messenger()->addStatus('Перемещение товаров аннулировано');
        }
        elseif ($op == 1) {
          $node->field_tp_utverzhdeno = 1;
          \Drupal::messenger()->addStatus('Перемещение товаров утверждено');
        }
      }

      $node->save();
    }
    $response->addCommand(new RedirectCommand('/node/' . $node->id()));
    unset($transaction);
    return $response;
  }

  /**
   * Обработка утверждения/аннулирования поступления товаров
   * @param Node $node
   */
  public function utverzhdeniePostupleniyaTovarov(Node $node, $op) {
    foreach ($node->get('field_tp_stroki') as $item) {
      if ($tovar = $item->entity->get('field_tp_tovar')->first()) {
        $tovar_nid = $tovar->entity->id();
        $sklad = $node->get('field_tp_sklad')->getString();
        $edinica_izereniya_tid = $item->entity->get('field_tp_edinica_izmereniya')->getString();
        $harakteristika_tid = $item->entity->get('field_tp_kharakteristika_tovara')->getString();
        $helper = \Drupal::service('trinion_tp.helper');
        $ostatok = $helper->getOstatokTovara($tovar_nid, $sklad, $node->id(), $harakteristika_tid, $edinica_izereniya_tid, TRUE);

        $current = $ostatok['kolichestvo'];
        $tovar_kolichestvo = $item->entity->get('field_tp_kolichestvo')->getString();

        if ($op) {
          $current = $current + $tovar_kolichestvo;
          $istoriya_op = '+';
        }
        else {
          $current = $current - $tovar_kolichestvo;
          $istoriya_op = '0';
        }

        $helper->createDvigenieOstatokTovara($ostatok['id'], $tovar_kolichestvo, $node->id(), $edinica_izereniya_tid, $istoriya_op);
        $helper->updateOstatokTovara($ostatok['id'], $current);
      }
    }
  }

  /**
   * Обработка утверждения/аннулирования отгрузки
   * @param Node $node
   */
  public function utverzhdenieOtgruzki(Node $node, $op) {
    if ($zakaz_kilenta = $node->get('field_tp_zakaz_klienta')->first())
      $zakaz_kilenta = $zakaz_kilenta->entity;

    $helper = \Drupal::service('trinion_tp.helper');

    foreach ($node->get('field_tp_stroki') as $stroka_uit) {
      $stroka_uit = $stroka_uit->entity;
      $tovar_nid = $stroka_uit->get('field_tp_tovar')->getString();
      $sklad_tid = $stroka_uit->get('field_tp_sklad')->getString();
      $harakteristika_tid = $stroka_uit->get('field_tp_kharakteristika_tovara')->getString();
      $edinica_izereniya_tid = $stroka_uit->get('field_tp_edinica_izmereniya')->getString();
      $kolichestvo = $stroka_uit->get('field_tp_kolichestvo')->getString();
      $ostatok = $helper->getOstatokTovara($tovar_nid, $sklad_tid, FALSE, $harakteristika_tid, $edinica_izereniya_tid);
      if ($zakaz_kilenta && ($rezerv = \Drupal::service('trinion_tp.helper')->getRezervTovaraByZakaz($zakaz_kilenta, $tovar_nid, $sklad_tid, $harakteristika_tid, $edinica_izereniya_tid))) {
        $rezerv_kolichestvo = $rezerv['kolichestvo'];
        $rezerv_kolichestvo_new = $rezerv_kolichestvo - $kolichestvo;
        if ($op) {
          $rezerv_kolichestvo = $rezerv_kolichestvo_new > 0 ? $rezerv_kolichestvo_new : 0;
          $ostatok['kolichestvo'] = $ostatok['kolichestvo'] - $kolichestvo;
          $helper->createDvigenieOstatokTovara($ostatok['id'], $kolichestvo, $node->id(), $edinica_izereniya_tid, '-');
          \Drupal::messenger()->addStatus('Отгрузка утверждена');
        }
        else {
          $rezerv_kolichestvo = $kolichestvo;
          $ostatok['kolichestvo'] = $ostatok['kolichestvo'] + $kolichestvo;
          $helper->createDvigenieOstatokTovara($ostatok['id'], $kolichestvo, $node->id(), $edinica_izereniya_tid, '0');
          \Drupal::messenger()->addStatus('Отгрузка аннулирована');
        }
        $helper->updateRezervTovara($rezerv['id'], $rezerv_kolichestvo);
      }
      $helper->updateOstatokTovara($ostatok['id'], $ostatok['kolichestvo']);
    }
  }

  /**
   * Обработка утверждения/аннулирования заказа клиента
   * @param Node $node
   * @param $op
   */
  public function utverzhdenieZakazaKlienta(Node $node, $op) {
    foreach ($node->get('field_tp_stroki') as $item) {
      $stroka = $item->entity;
      if ($tovar = $stroka->get('field_tp_tovar')->first()) {
        $tovar = $tovar->entity;
        $sklad = $item->entity->get('field_tp_sklad')->getString();
        $edinica_izereniya_tid = $item->entity->get('field_tp_edinica_izmereniya')->getString();
        $harakteristika_tid = $item->entity->get('field_tp_kharakteristika_tovara')->getString();
        switch ($op) {
          case 0:
            if ($rezerv = \Drupal::service('trinion_tp.helper')->getRezervTovaraByZakaz($node, $tovar->id(), $sklad, $harakteristika_tid, $edinica_izereniya_tid))
              $rezerv->delete();
            break;
          case 1:
            $kolichestvo = $stroka->get('field_tp_kolichestvo')->getString();
            \Drupal::service('trinion_tp.helper')->createRezervTovara($tovar, $sklad, $harakteristika_tid, $edinica_izereniya_tid, $kolichestvo, $node->id());
            break;
        }
      }
    }
  }

  /**
   * Обработка утверждения/аннулирования перемещения товаров между складами
   * @param Node $node
   * @param $op
   */
  public function utverzhdeniePeremesheniyaTovarov(Node $node, $op) {
    $helper = \Drupal::service('trinion_tp.helper');
    foreach ($node->get('field_tp_stroki') as $item) {
      $stroka = $item->entity;
      if ($tovar = $stroka->get('field_tp_tovar')->first()) {
        $tovar = $tovar->entity;
        $tovar_nid = $tovar->id();
        $kolichestvo = $item->entity->get('field_tp_kolichestvo')->getString();
        $sklad = $node->get('field_tp_sklad')->getString();
        $edinica_izereniya_tid = $item->entity->get('field_tp_edinica_izmereniya')->getString();
        $harakteristika_tid = $item->entity->get('field_tp_kharakteristika_tovara')->getString();
        $ostatok = $helper->getOstatokTovara($tovar_nid, $sklad, FALSE, $harakteristika_tid, $edinica_izereniya_tid, TRUE);
        switch ($op) {
          case 0:
            $current = $ostatok['kolichestvo'] - $kolichestvo;
            $helper->createDvigenieOstatokTovara($ostatok['id'], -1 * $kolichestvo, $node->id(), $edinica_izereniya_tid, '0');
            break;
          case 1:
            $current = $ostatok['kolichestvo'] + $kolichestvo;
            $helper->createDvigenieOstatokTovara($ostatok['id'], $kolichestvo, $node->id(), $edinica_izereniya_tid, '1');
            break;
        }
        $helper->updateOstatokTovara($ostatok['id'], $current);
      }
    }
  }

  /**
   * Обработка утверждения/аннулирования требования-накладной
   * @param Node $node
   * @param $op
   */
  public function utverzhdenieTrebovaniyaNakladnoy(Node $node, $op) {
    $podrazdelenie = $node->get('field_tp_podrazdelenie')->getString();
    $helper = \Drupal::service('trinion_tp.helper');
    foreach ($node->get('field_tp_stroki') as $item) {
      $stroka = $item->entity;
      if ($tovar = $stroka->get('field_tp_tovar')->first()) {
        $tovar = $tovar->entity;
        $tovar_nid = $tovar->id();
        $kolichestvo = $item->entity->get('field_tp_kolichestvo')->getString();
        $sklad = $node->get('field_tp_sklad')->getString();
        $edinica_izereniya_tid = $item->entity->get('field_tp_edinica_izmereniya')->getString();
        $harakteristika_tid = $item->entity->get('field_tp_kharakteristika_tovara')->getString();
        $ostatok = $helper->getOstatokTovara($tovar_nid, $sklad, FALSE, $harakteristika_tid, $edinica_izereniya_tid, TRUE);
        $ostatok_v_proizvodstve_node = $helper->getOstatokVProizvodstveTovaraNode($tovar_nid, $podrazdelenie, $harakteristika_tid, $edinica_izereniya_tid, TRUE);
        switch ($op) {
          case 0:
            $current = $ostatok['kolichestvo'] + $kolichestvo;
            $helper->createDvigenieOstatokTovara($ostatok['id'], -1 * $kolichestvo, $node->id(), $edinica_izereniya_tid, '0');
            $ostatok_v_proizvodstve_node->field_tp_kolichestvo = $ostatok_v_proizvodstve_node->get('field_tp_kolichestvo')->getString() - $kolichestvo;
            $ostatok_v_proizvodstve_node->save();
            break;
          case 1:
            $current = $ostatok['kolichestvo'] - $kolichestvo;
            $helper->createDvigenieOstatokTovara($ostatok['id'], -1 * $kolichestvo, $node->id(), $edinica_izereniya_tid, '-');
            $ostatok_v_proizvodstve_node->field_tp_kolichestvo = $ostatok_v_proizvodstve_node->get('field_tp_kolichestvo')->getString() + $kolichestvo;
            $ostatok_v_proizvodstve_node->save();
            break;
        }
        $helper->updateOstatokTovara($ostatok['id'], $current);
      }
    }
  }

  /**
   * Обработка утверждения/аннулирования выпуска продукции
   * @param Node $node
   * @param $op
   */
  public function utverzhdenieVipuskaProdukcii(Node $node, $op) {
    $helper = \Drupal::service('trinion_tp.helper');
    foreach ($node->get('field_tp_stroki') as $item) {
      $stroka = $item->entity;
      if ($tovar = $stroka->get('field_tp_tovar')->first()) {
        $tovar = $tovar->entity;
        $tovar_nid = $tovar->id();
        $kolichestvo = $item->entity->get('field_tp_kolichestvo')->getString();
        $sklad = $node->get('field_tp_sklad')->getString();
        $edinica_izereniya_tid = $item->entity->get('field_tp_edinica_izmereniya')->getString();
        $harakteristika_tid = $item->entity->get('field_tp_kharakteristika_tovara')->getString();
        $ostatok = $helper->getOstatokTovara($tovar_nid, $sklad, FALSE, $harakteristika_tid, $edinica_izereniya_tid, TRUE);
        switch ($op) {
          case 0:
            $current = $ostatok['kolichestvo'] - $kolichestvo;
            $helper->createDvigenieOstatokTovara($ostatok['id'], -1 * $kolichestvo, $node->id(), $edinica_izereniya_tid, '0');
            break;
          case 1:
            $current = $ostatok['kolichestvo'] + $kolichestvo;
            $helper->createDvigenieOstatokTovara($ostatok['id'], $kolichestvo, $node->id(), $edinica_izereniya_tid, '+');
            break;
        }
        $helper->updateOstatokTovara($ostatok['id'], $current);
      }
    }
  }

  /**
   * Обработка утверждения/аннулирования отправленного платежа
   * @param Node $node
   * @param $op
   */
  public function utverzhdenieOtpravlennogoPlatezha(Node $node, $op) {
    $scheta_to_recalc = [];
    foreach ($node->get('field_tp_stroki') as $item)
      if ($item->entity)
        $scheta_to_recalc[] = $item->entity->get('field_tp_schet')->getString();
    \Drupal::service('trinion_tp.helper')->updateSchetProdavtsa($scheta_to_recalc, $node->id(), $op ? 'add' : 'delete');
  }

  /**
   * Обработка утверждения/аннулирования получченного платежа
   * @param Node $node
   * @param $op
   */
  public function utverzhdeniePoluchennogoPlatezha(Node $node, $op) {
    $scheta_to_recalc = [];
    foreach ($node->get('field_tp_stroki') as $item)
      if ($item->entity)
        $scheta_to_recalc[] = $item->entity->get('field_tp_schet')->getString();
    \Drupal::service('trinion_tp.helper')->updateSchet($scheta_to_recalc, $node->id(), $op ? 'add' : 'delete');
  }
}
