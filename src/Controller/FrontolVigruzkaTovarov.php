<?php

namespace Drupal\trinion_tp\Controller;

use Drupal\Core\Controller\ControllerBase;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\node\Entity\Node;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpFoundation\Response;

/**
 * Выгрузка товаров и услуг во Frontol
 */
class FrontolVigruzkaTovarov extends ControllerBase {

  /**
   * The entity type manager.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * The controller constructor.
   *
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entity_type_manager
   *   The entity type manager.
   */
  public function __construct(EntityTypeManagerInterface $entity_type_manager) {
    $this->entityTypeManager = $entity_type_manager;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('entity_type.manager')
    );
  }

  public function generate() {
    $text = '##@@&&' . PHP_EOL;
    $text .= '#' . PHP_EOL;
    $text .= '$$$ADDTAXGROUPS' . PHP_EOL;
    $text .= '8;Без НДС;Без НДС' . PHP_EOL;
    $text .= '$$$ADDQUANTITY' . PHP_EOL;

    $global_print_group = \Drupal::config('trinion_tp.settings')->get('frontol_global_print_group');
    if (empty($global_print_group))
      $global_print_group = 1;
    $product_bundles = \Drupal::config('trinion_tp.settings')->get('product_bundles');
    if ($product_bundles) {
      $query = \Drupal::entityQuery('node')
        ->condition('type', $product_bundles, 'IN');
      $tovar_nids = $query->accessCheck(TRUE)->execute();

      foreach ($tovar_nids as $tovar_nid) {
        $tovar = $this->entityTypeManager->getStorage('node')->load($tovar_nid);

        $query = \Drupal::entityQuery('node')
          ->condition('type', 'cena')
          ->condition('field_tp_tovar', $tovar_nid)
          ->condition('field_tp_tip_ceny', \Drupal::config('trinion_tp.settings')->get('roznichnaya_cena_tid'))
          ->range(0, 1);
        $cena_res = $query->accessCheck()->execute();
        $harakteristiki = [];
        $price = 0;
        if ($cena_res) {
          $cena_nid = reset($cena_res);
          if ($cena_node = Node::load($cena_nid)) {
            $price = $cena_node->get('field_tp_cena')->getString();
            foreach ($cena_node->get('field_tp_kharakteristika_tovara') as $harakteristika)
              if ($harakteristika && $harakteristika->entity)
                $harakteristiki[$harakteristika->entity->id()] = $harakteristika->entity->label();
            ksort($harakteristiki);
          }
        }

        $name = $tovar->label();
        $item_id = $tovar->id();
        if ($harakteristiki) {
          $name .= ' ' . implode(', ', $harakteristiki);
//          $item_id .= '-' . implode('-', array_keys($harakteristiki));
        }

        $name = mb_substr($name, 0, 126);
        $name = str_replace(';', ' ', $name);

        $flags = [0, 1, 0, 1, 0, 0, 1, 1, 1, 0, 0, 0, 0, 0, ];

        $print_group = $global_print_group;

        $fields = [
          1 => '1' . str_pad($item_id, 8, '0', STR_PAD_LEFT),// . ($harakteristiki ? '-' . implode('-', $harakteristiki) : ''),
          2 => '',
          3 => $name,
          4 => $name,
          5 => (float)$price,
          6 => '',
          7 => '',
          8 => implode(',', $flags),
          9 => '',
          10 => '',
          11 => '',
          12 => '',
          13 => '',
          14 => '',
          15 => '',
          16 => '',
          17 => '',
          18 => '',
          19 => '',
          20 => '',
          21 => '',
          22 => '',
          23 => '8',
          24 => '',
          25 => '',
          26 => $name,
          27 => '',
          28 => '',
          29 => '',
          30 => '',
          31 => '',
          32 => '',
          33 => '',
          34 => '',
          35 => '',
          36 => '',
          37 => '',
          38 => '',
          39 => (string)$print_group,
          40 => '',
          41 => '',
          42 => '',
          43 => '',
          44 => '',
          45 => '',
          46 => '',
          47 => '',
          48 => '',
          49 => '',
          50 => '',
          51 => '',
          52 => '',
          53 => '',
          54 => '',
        ];
        $text .= implode(';', $fields) . PHP_EOL;

      }
      $text = iconv('UTF-8', 'Windows-1251//IGNORE', $text);
    }
    return $text;
  }

  /**
   * Builds the response.
   */
  public function build() {
//    $text = chr(239) . chr(187) . chr(191);

    $text = $this->generate();
    $response = new Response();
    $response->headers->set('Content-type', 'text/plain');
    $response->headers->set('Content-length', strlen($text));
    $response->headers->set('Content-Disposition', 'attachment; filename="services.txt"');
    $response->setContent($text);

    return $response;
  }

  public function saveToFile() {
    $text = $this->generate();
    $save_path = \Drupal::config('trinion_tp.settings')->get('frontol_save_tovari_path');
    $save_flag_path = \Drupal::config('trinion_tp.settings')->get('frontol_save_tovari_flag_path');
    $file_name = DRUPAL_ROOT . '/' . $save_path;
    $file_flag_name = DRUPAL_ROOT . '/' . $save_flag_path;
    $fp = fopen($file_name, 'w');
    fwrite($fp, $text);
    fclose($fp);
    $fp = fopen($file_flag_name, 'w');
    fwrite($fp, time());
    fclose($fp);
    \Drupal::messenger()->addMessage('Выгрузка сохранена в файл ' . $file_name . ' . Онлайн-касса скоро должна будет этот файл обработать.');
    return [];
  }

}
