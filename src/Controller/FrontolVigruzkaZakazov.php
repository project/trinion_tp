<?php

namespace Drupal\trinion_tp\Controller;

use Drupal\Core\Controller\ControllerBase;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\node\Entity\Node;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Выгруза заказов во Frontol
 */
class FrontolVigruzkaZakazov {

  /**
   * Создания файла выгрузки заказа во фронтол
   */
  public function createOrderFile(Node $node) {
    $order_id = $node->label();
    $date = $node->get('field_tp_data')->getString();

    $date = date('d.m.Y', strtotime($date));
    $time = '00:00';
    $total = $node->get('field_tp_itogo')->getString();

    $order_id = "0000{$order_id}";
    $data = "{$order_id};1;{$date};{$time};{$total};;{$order_id};1;;;\n";
    foreach ($node->get('field_tp_stroki') as $item) {
      $tovar = $item->entity;

      $harakteristiki = [];
      foreach ($tovar->get('field_tp_kharakteristika_tovara') as $harakteristika)
        if ($harakteristika && $harakteristika->entity)
          $harakteristiki[$harakteristika->entity->id()] = $harakteristika->entity->label();
      ksort($harakteristiki);
      $name = $tovar->label();
      $item_id = $tovar->get('field_tp_tovar')->first()->entity->id();
      if ($harakteristiki) {
        $name .= ' ' . implode(', ', $harakteristiki);
//        $item_id .= '-' . implode('-', array_keys($harakteristiki));
      }

      $kolichestvo = $tovar->get('field_tp_kolichestvo')->getString();
      $itogo = $tovar->get('field_tp_itogo')->getString();
      $cena_edinici = $itogo / $kolichestvo;
      $data .= "1;" . '1' . str_pad($item_id, 8, '0', STR_PAD_LEFT) . ";1;{$cena_edinici};{$kolichestvo}\n";
    }
    $data = iconv('UTF-8', 'Windows-1251//IGNORE', $data);
    $save_path = \Drupal::config('trinion_tp.settings')->get('frontol_save_zakazi_path');
    $save_path = preg_replace('/\/$/', '', $save_path);
    $file_name = DRUPAL_ROOT . '/' . $save_path . "/order{$order_id}.opn";
    $fp = fopen($file_name, 'w');
    fwrite($fp, $data);
    fclose($fp);
  }

}
