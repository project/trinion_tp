<?php

namespace Drupal\trinion_tp\Controller;

use Dompdf\Dompdf;
use Drupal\Core\Ajax\AjaxResponse;
use Drupal\Core\Ajax\AlertCommand;
use Drupal\Core\Ajax\RedirectCommand;
use Drupal\Core\Controller\ControllerBase;
use Drupal\node\Entity\Node;

/**
 * Создание Полученного платежа
 */
class SozdaniePoluchennogoPlatezha extends ControllerBase {

  /**
   * Создание Полученного платежа
   * @param array $scheta Массив массивов с ключами 'schet', 'summa'
   */
  public function createPoluchenniyPlatezh(array $scheta, $num_vh_doc, $sistema_oplati, string $comment = '', $date = NULL) {
    $uid = \Drupal::currentUser()->id();
    $schet = reset($scheta);
    $schet = $schet['schet'];
    $platezh_data = [
      'type' => 'poluchennyy_platezh',
      'title' => $this->getNomer(),
      'uid' => $uid,
      'status' => 1,
//      'field_tp_data' => $schet->get('field_tp_data')->getString(),
      'field_tp_opisanie' => $comment,
      'field_tp_otvetstvennyy' => ['target_id' => $schet->get('field_tp_otvetstvennyy')->getString()],
      'field_tp_platelschik' => ['target_id' => $schet->get('field_tp_schet_dlya')->getString()],
      'field_tp_nomer_vkh_plat' => $num_vh_doc,
      'field_tp_itogo' => 0,
      'field_tp_stroki' => [],
    ];
    if ($org = $schet->get('field_tp_organizaciya')->getString())
      $platezh_data['field_tp_organizaciya'] = ['target_id' => $org];
    if ($sistema_oplati)
      $platezh_data['field_tp_platezhnaya_sistema'] = ['target_id' => $sistema_oplati];

    foreach ($scheta as $schet_data) {
      $schet = $schet_data['schet'];
      $stroka_data = [
        'type' => 'tp_stroka_schet_klienta_k_oplate',
        'title' => '2',
        'field_tp_oplachennaya_summa' => $schet_data['summa'],
        'field_tp_schet' => ['target_id' => $schet->id()],
        'uid' => $uid,
        'status' => 1,
      ];
      $stroka = Node::create($stroka_data);
      $stroka->save();
      $platezh_data['field_tp_stroki'][] = ['target_id' => $stroka->id()];
      $platezh_data['field_tp_itogo'] += $schet_data['summa'];
    }
    $platezh_data['field_tp_utverzhdeno'] = 0;
    $platezh = Node::create($platezh_data);
    $platezh->save();
    return $platezh;
  }

  public function getNomer() {
    return \Drupal::service('trinion_tp.helper')->getNextDocumentNumber('poluchennyy_platezh');
  }
}

