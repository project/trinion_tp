<?php

namespace Drupal\zoo_mod\Form;

use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\field\Entity\FieldStorageConfig;
use Drupal\file\Entity\File;
use Drupal\node\Entity\Node;
use Drupal\taxonomy\Entity\Term;

/**
 * Форма загрузки платежей Клиент-банк
 */
class ClientBankImportPaymentsForm extends FormBase {

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'zoo_mod_client_bank_import_payments';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $form['file'] = [
      '#type' => 'managed_file',
      '#title' => 'Файл',
//      '#required' => TRUE,
      '#upload_validators' => [
        'file_validate_extensions' => ['txt'],
      ],
    ];

    $form['actions'] = [
      '#type' => 'actions',
    ];
    $form['actions']['submit'] = [
      '#type' => 'submit',
      '#value' => 'Загрузить',
    ];

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function validateForm(array &$form, FormStateInterface $form_state) {
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $file = $form_state->getValue('file');
    if (empty($file[0]))
      return;
    $file = File::load($file[0]);

    $file_path = $file->get('uri')->getString();
    $str = file_get_contents($file_path);
    $str = iconv('Windows-1251', 'UTF-8', $str);
    preg_match_all('/СекцияДокумент=.*?КонецДокумента/s', $str, $matches);
    $count_created = 0;
    foreach ($matches[0] as $match) {
      $lines = explode("\n", $match);
      $data = ['other' => ''];
      foreach ($lines as $line) {
        if ($line == 'КонецДокумента')
          break;
        list($param, $value) = explode('=', $line);
        $value = trim($value);
        switch ($param) {
          case 'СекцияДокумент':
            $data['client_bank_type_doc'] = $value;
            break;
          case 'Номер':
            $data['nomer'] = $value;
            break;
          case 'Дата':
            $data['document_date'] = $value;
            break;
          case 'Сумма':
            $data['inc_sum'] = $value;
            break;
          case 'Плательщик':
            $data['payer_name'] = $value;
            break;
          case 'ПлательщикИНН':
            $data['payer_inn'] = $value;
            break;
          case 'ПлательщикКПП':
            $data['payer_kpp'] = $value;
            break;
          case 'ПлательщикРасчСчет':
            $data['payer_schet'] = $value;
            break;
          case 'ПлательщикБанк1':
            $data['payer_bank'] = $value;
            break;
          case 'ПлательщикБИК':
            $data['payer_bik'] = $value;
            break;
          case 'ПлательщикКорсчет':
            $data['payer_kor_schet'] = $value;
            break;
          case 'Получатель':
            $data['recepient_name'] = $value;
            break;
          case 'ПолучательИНН':
            $data['recepient_inn'] = $value;
            break;
          case 'ПолучательКПП':
            $data['recepient_kpp'] = $value;
            break;
          case 'ПолучательРасчСчет':
            $data['recepient_schet'] = $value;
            break;
          case 'ПолучательБанк1':
            $data['recepient_bank'] = $value;
            break;
          case 'ПолучательБИК':
            $data['recepient_bik'] = $value;
            break;
          case 'ПолучательКорсчет':
            $data['recepient_kor_schet'] = $value;
            break;
          case 'НазначениеПлатежа':
            $data['destination_reason'] = $value;
            break;
          default:
            $data['other'] .= $param . ': ' . $value . PHP_EOL;
        }
      }

      $payer_tid = $this->createContragent($data, 'payer');
      $recepient_tid = $this->createContragent($data, 'recepient');
      if (empty($payer_tid)) {
        \Drupal::messenger()->addError('Не удалось создать Плательщика: ' . implode("\n", $lines));
        return;
      }
      if (empty($recepient_tid)) {
        \Drupal::messenger()->addError('Не удалось создать Получателя: ' . implode("\n", $lines));
        return;
      }

      $client_bank_type_doc_tid = $this->createDocType($data['client_bank_type_doc']);
      $query = \Drupal::entityQuery('node')
        ->accessCheck(TRUE)
        ->condition('type', 'client_bank_payment')
        ->condition('field_nomer', $data['nomer'])
        ->condition('field_payer', $payer_tid)
        ->condition('field_recepient', $recepient_tid)
        ->condition('field_client_bank_type_doc', $client_bank_type_doc_tid);
      $doc = $query->execute();
      if (empty($doc)) {
        $node = Node::create([
          'type' => 'client_bank_payment',
          'title' => $data['client_bank_type_doc'] . ' ' . $data['document_date'],
          'uid' => 1,
          'status' => 1,
          'field_client_bank_type_doc' => $client_bank_type_doc_tid,
          'field_document_date' => date('Y-m-d', strtotime($data['document_date'])),
          'field_call_comment' => $data['destination_reason'],
          'field_nomer' => $data['nomer'],
          'field_payer' => $payer_tid,
          'field_recepient' => $recepient_tid,
          'field_description' => $data['other'],
          'field_inc_sum' => $data['inc_sum'],
        ]);
        $node->save();
        $count_created++;
      }
    }
    \Drupal::messenger()->addStatus('Создано ' . $count_created . ' документов');
  }

  public function createContragent($data, $type) {
    if (!isset($data[$type . '_inn']) || trim($data[$type . '_inn']) == '') {
      \Drupal::messenger()->addError("У контрагента '{$data[$type . '_name']}' не заполнен ИНН. Документ {$data['nomer']}.");
      return;
    }
    if (!isset($data[$type . '_kpp']) || trim($data[$type . '_kpp']) == '') {
      \Drupal::messenger()->addError("У контрагента '{$data[$type . '_name']}' не заполнен КПП. Документ {$data['nomer']}.");
      return;
    }
    $vid = 'contragent_client_bank';
    $query = \Drupal::entityQuery('taxonomy_term')
      ->accessCheck(TRUE)
      ->condition('vid', $vid)
      ->condition('field_inn', $data[$type . '_inn'])
      ->condition('field_kpp', $data[$type . '_kpp']);
    $tid = $query->execute();
    if (empty($tid)) {
      $term = Term::create([
        'vid' => $vid,
        'name' => $data[$type . '_name'],
        'field_bank' => $data[$type . '_bank'],
        'field_bik' => $data[$type . '_bik'],
        'field_inn' => $data[$type . '_inn'],
        'field_kpp' => $data[$type . '_kpp'],
        'field_kor_schet' => $data[$type . '_kor_schet'],
        'field_schet' => $data[$type . '_schet'],
      ]);
      $term->save();
      $tid = $term->id();
    }
    else
      $tid = reset($tid);
    return $tid;
  }

  public function createDocType($name) {
    if (empty($name))
      return;
    $vid = 'client_bank_type_doc';
    $query = \Drupal::entityQuery('taxonomy_term')
      ->accessCheck(TRUE)
      ->condition('vid', $vid)
      ->condition('name', $name);
    $tid = $query->execute();
    if (empty($tid)) {
      $term = Term::create([
        'vid' => $vid,
        'name' => $name,
      ]);
      $term->save();
      $tid = $term->id();
    }
    else
      $tid = reset($tid);
    return $tid;
  }

}
