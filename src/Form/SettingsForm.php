<?php

namespace Drupal\trinion_tp\Form;

use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\node\Entity\Node;

/**
 * Configure Trinion tp settings for this site.
 */
class SettingsForm extends ConfigFormBase {

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'trinion_tp_settings';
  }

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return ['trinion_tp.settings'];
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {

    $options = self::getProductNodeTypes();
    $config = $this->config('trinion_tp.settings');
    $product_bundles = $config->get('product_bundles');
    $form['product_bundles'] = [
      '#type' => 'checkboxes',
      '#title' => 'Типы материалов - товары',
      '#options' => $options,
      '#default_value' => $product_bundles ? $product_bundles : [],
      '#description' => 'Тут отображаются только типы материалов, которые имеют поле field_tp_artikul, field_tp_edinica_izmereniya, field_tp_nds',
    ];

    $form['harakteristiki'] = [
      '#type' => 'checkbox',
      '#title' => 'У товаров есть характеристики',
      '#default_value' => $config->get('harakteristiki'),
    ];

    $form['kommercheskoe_predlozhenie_start_nomer'] = [
      '#type' => 'number',
      '#title' => 'Цифра с которой начнется нумерация Коммерческих предложений',
      '#default_value' => $config->get('kommercheskoe_predlozhenie_start_nomer'),
    ];

    $form['zakaz_klienta_start_nomer'] = [
      '#type' => 'number',
      '#title' => 'Цифра с которой начнется нумерация Заказов покупателя',
      '#default_value' => $config->get('zakaz_klienta_start_nomer'),
    ];

    $form['schet_start_nomer'] = [
      '#type' => 'number',
      '#title' => 'Цифра с которой начнется нумерация Счетов покупателя',
      '#default_value' => $config->get('schet_start_nomer'),
    ];

    $form['zakaz_postavschiku_start_nomer'] = [
      '#type' => 'number',
      '#title' => 'Цифра с которой начнется нумерация Заказов поставщику',
      '#default_value' => $config->get('zakaz_postavschiku_start_nomer'),
    ];

    $form['poluchennyy_platezh_start_nomer'] = [
      '#type' => 'number',
      '#title' => 'Цифра с которой начнется нумерация Полученных платежей',
      '#default_value' => $config->get('poluchennyy_platezh_start_nomer'),
    ];

    $form['otpravlenniy_platezh_start_nomer'] = [
      '#type' => 'number',
      '#title' => 'Цифра с которой начнется нумерация Отправленных платежей',
      '#default_value' => $config->get('otpravlenniy_platezh_start_nomer'),
    ];

    $form['schet_postavschika_start_nomer'] = [
      '#type' => 'number',
      '#title' => 'Цифра с которой начнется нумерация Счетов поставщика',
      '#default_value' => $config->get('schet_postavschika_start_nomer'),
    ];

    $form['otgruzka_start_nomer'] = [
      '#type' => 'number',
      '#title' => 'Цифра с которой начнется нумерация Отргрузок',
      '#default_value' => $config->get('otgruzka_start_nomer'),
    ];

    $form['frontol'] = [
      '#type' => 'fieldset',
      '#title' => 'Интеграция с Frontol',
    ];

    $form['frontol']['frontol_global_print_group'] = [
      '#type' => 'textfield',
      '#title' => 'Группа печати по-умолчанию',
      '#default_value' => $config->get('frontol_global_print_group'),
    ];

    $form['frontol']['frontol_save_tovari_path'] = [
      '#type' => 'textfield',
      '#title' => 'Файл для сохранения выгрузки товаров',
      '#description' => 'Путь относительно DOCUMENT_ROOT.',
      '#default_value' => $config->get('frontol_save_tovari_path'),
    ];

    $form['frontol']['frontol_save_tovari_flag_path'] = [
      '#type' => 'textfield',
      '#title' => 'Файл флаг для сохранения выгрузки товаров',
      '#description' => 'Путь относительно DOCUMENT_ROOT.',
      '#default_value' => $config->get('frontol_save_tovari_flag_path'),
    ];

    $form['frontol']['frontol_save_zakazi_path'] = [
      '#type' => 'textfield',
      '#title' => 'Путь для сохранения выгрузки заказов',
      '#description' => 'Путь относительно DOCUMENT_ROOT. Будет сохранен в файл %ид_заказа.txt',
      '#default_value' => $config->get('frontol_save_zakazi_path'),
    ];

//    $form['frontol']['frontol_ftp_server'] = [
//      '#type' => 'textfield',
//      '#title' => 'Адрес FTP сервера',
//      '#default_value' => $config->get('frontol_ftp_server'),
//    ];
//
//    $form['frontol']['frontol_ftp_user_name'] = [
//      '#type' => 'textfield',
//      '#title' => 'FTP пользователь',
//      '#default_value' => $config->get('frontol_ftp_user_name'),
//    ];
//
//    $form['frontol']['frontol_ftp_user_pass'] = [
//      '#type' => 'textfield',
//      '#title' => 'Пароль FTP пользователя',
//      '#default_value' => $config->get('frontol_ftp_user_pass'),
//    ];

    $form['frontol']['frontol_get_payment_flag'] = [
      '#type' => 'textfield',
      '#title' => 'Файл флаг для инициализации выгрузки оплат во Frontol',
      '#description' => 'Путь относительно DOCUMENT_ROOT.',
      '#default_value' => $config->get('frontol_get_payment_flag'),
    ];

    $form['frontol']['frontol_get_payment_file'] = [
      '#type' => 'textfield',
      '#title' => 'Файл выгрузки оплат из Frontol',
      '#description' => 'Путь относительно DOCUMENT_ROOT.',
      '#default_value' => $config->get('frontol_get_payment_file'),
    ];

    return parent::buildForm($form, $form_state);
  }

  public function validateForm(array &$form, FormStateInterface $form_state) {
    if ($save_path = $form_state->getValue('frontol_save_tovari_path')) {
      $save_path = preg_replace('/\/[^\/]+$/', '', $save_path);
      $save_path = DRUPAL_ROOT . '/' . $save_path;
      if (!is_writable($save_path))
        $form_state->setErrorByName('frontol_save_tovari_path', $save_path . ' не доступен для записи');
    }

    if ($save_path = $form_state->getValue('frontol_save_tovari_flag_path')) {
      $save_path = preg_replace('/\/[^\/]+$/', '', $save_path);
      $save_path = DRUPAL_ROOT . '/' . $save_path;
      if (!is_writable($save_path))
        $form_state->setErrorByName('frontol_save_tovari_flag_path', $save_path . ' не доступен для записи');
    }

    if ($save_path = $form_state->getValue('frontol_get_payment_flag')) {
      $save_path = preg_replace('/\/[^\/]+$/', '', $save_path);
      $save_path = DRUPAL_ROOT . '/' . $save_path;
      if (!is_writable($save_path))
        $form_state->setErrorByName('frontol_get_payment_flag', $save_path . ' не доступен для записи');
    }

    if ($save_path = $form_state->getValue('frontol_save_zakazi_path')) {
      $save_path = preg_replace('/\/$/', '', $save_path);
      $save_path = DRUPAL_ROOT . '/' . $save_path;
      if (!is_writable($save_path))
        $form_state->setErrorByName('frontol_save_zakazi_path', $save_path . ' не доступен для записи');
    }
    parent::validateForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $product_bundles = [];
    foreach ($form_state->getValue('product_bundles') as $bundle => $enable)
      if ($enable)
        $product_bundles[$bundle] = $bundle;
    $this->config('trinion_tp.settings')
      ->set('product_bundles', $product_bundles)
      ->set('frontol_global_print_group', $form_state->getValue('frontol_global_print_group'))
      ->set('frontol_save_zakazi_path', $form_state->getValue('frontol_save_zakazi_path'))
      ->set('frontol_save_tovari_path', $form_state->getValue('frontol_save_tovari_path'))
      ->set('frontol_save_tovari_flag_path', $form_state->getValue('frontol_save_tovari_flag_path'))
      ->set('frontol_get_payment_flag', $form_state->getValue('frontol_get_payment_flag'))
      ->set('frontol_get_payment_file', $form_state->getValue('frontol_get_payment_file'))
      ->set('zakaz_klienta_start_nomer', $form_state->getValue('zakaz_klienta_start_nomer'))
      ->set('schet_start_nomer', $form_state->getValue('schet_start_nomer'))
      ->set('zakaz_postavschiku_start_nomer', $form_state->getValue('zakaz_postavschiku_start_nomer'))
      ->set('poluchennyy_platezh_start_nomer', $form_state->getValue('poluchennyy_platezh_start_nomer'))
      ->set('otpravlenniy_platezh_start_nomer', $form_state->getValue('otpravlenniy_platezh_start_nomer'))
      ->set('kommercheskoe_predlozhenie_start_nomer', $form_state->getValue('kommercheskoe_predlozhenie_start_nomer'))
      ->set('schet_postavschika_start_nomer', $form_state->getValue('schet_postavschika_start_nomer'))
      ->set('otgruzka_start_nomer', $form_state->getValue('otgruzka_start_nomer'))
      ->set('harakteristiki', $form_state->getValue('harakteristiki'))
      ->save();
      drupal_flush_all_caches();
    parent::submitForm($form, $form_state);
  }

  /**
   * Типы нод - товары
   * @return array
   * @throws \Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException
   * @throws \Drupal\Component\Plugin\Exception\PluginNotFoundException
   */
  public static function getProductNodeTypes() {
    $types = \Drupal::entityTypeManager()
      ->getStorage('node_type')
      ->loadMultiple();
    $items = [];
    foreach ($types as $key => $type) {
      if ($key == 'tp_stroka_dokumenta_uit')
        continue;
      $fields = \Drupal::service('entity_field.manager')->getFieldDefinitions('node', $key);
      if (isset($fields['field_tp_artikul'], $fields['field_tp_edinica_izmereniya'], $fields['field_tp_nds'])) {
        $items[$key] = $type->label();
      }
    }
    return $items;
  }
}
