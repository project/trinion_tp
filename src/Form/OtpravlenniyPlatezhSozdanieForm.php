<?php

namespace Drupal\trinion_tp\Form;

use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;
use Symfony\Component\HttpFoundation\RedirectResponse;

/**
 * Форма выбора Компании получателя при создании Отправленного платежа
 */
class OtpravlenniyPlatezhSozdanieForm extends FormBase {

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'trinion_tp_otpravlenniy_platezh_sozdanie';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $form['companiya'] = [
      '#type' => 'entity_autocomplete',
      '#title' => 'Компания',
      '#target_type' => 'node',
      '#selection_settings' => [
        'target_bundles' => ['kompanii'],
      ],
      '#required' => TRUE,
    ];

    $form['actions'] = [
      '#type' => 'actions',
    ];
    $form['actions']['submit'] = [
      '#type' => 'submit',
      '#value' => 'Далее',
    ];

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $form_state->setRedirect('node.add', ['node_type' => 'otpravlennyy_platezh'], ['query' => ['companiya' => $form_state->getValue('companiya')]]);
  }
}
