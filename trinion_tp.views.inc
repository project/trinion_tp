<?php

/**
 * Implements hook_views_data_alter().
 *
 * Alter DB tables defined via hook_views_data().
 */
function trinion_tp_views_data_alter(array &$data) {
  $data['node']['trinion_tp_jivoy_ostatok_tovara'] = [
    'title' => 'Живой остаток',
    'field' => [
      'title' => 'Живой остаток',
      'id' => 'trinion_tp_jivoy_ostatok_tovara',
    ],
  ];
  $data['node']['trinion_tp_schet_state'] = [
    'title' => t('Sales order state'),
    'field' => [
      'title' => t('Sales order state'),
      'id' => 'trinion_tp_schet_state',
    ],
  ];
  $data['node']['trinion_tp_schet_payed_status'] = [
    'title' => t('Sales order payed status'),
    'field' => [
      'title' => t('Sales order payed status'),
      'id' => 'trinion_tp_schet_payed_status',
    ],
  ];
  $data['node']['trinion_tp_schet_otgruzka_status'] = [
    'title' => t('Sales order shipment status'),
    'field' => [
      'title' => t('Sales order shipment status'),
      'id' => 'trinion_tp_schet_otgruzka_status',
    ],
  ];
  $data['node']['trinion_tp_schet_postavshika_sostoyanie_oplati'] = [
    'title' => t('Bill payment state'),
    'field' => [
      'title' => t('Bill payment state'),
      'id' => 'trinion_tp_schet_postavshika_sostoyanie_oplati',
    ],
  ];
  $data['node']['trinion_tp_schet_postavshika_oplacheno'] = [
    'title' => t('Bill payed sum'),
    'field' => [
      'title' => t('Bill payed sum'),
      'id' => 'trinion_tp_schet_postavshika_oplacheno',
    ],
  ];
  $data['node']['trinion_tp_schet_klienta_sostoyanie_oplati'] = [
    'title' => t('Invoice payment state'),
    'field' => [
      'title' => t('Invoice payment state'),
      'id' => 'trinion_tp_schet_klienta_sostoyanie_oplati',
    ],
  ];
  $data['node']['trinion_tp_schet_klienta_oplacheno'] = [
    'title' => t('Invoice payed sum'),
    'field' => [
      'title' => t('Invoice payed sum'),
      'id' => 'trinion_tp_schet_klienta_oplacheno',
    ],
  ];
}
