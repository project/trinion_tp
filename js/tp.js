(function ($, Drupal) {
  Drupal.behaviors.tp = {
    attach: function (context, settings) {
      recalc();
      $("#edit-field-tp-organizaciya").change(function (){
        changeQueryHash($(this), 'org');
      });
      $(".pole_kompanii").change(function (){
        changeQueryHash($(this), 'comp');
      });
      $(".pole_sklada").change(function (){
        $(".stroki-table .harakteristika").each(function(){
          $(this).trigger("change");
        });
      });
      function changeQueryHash(el, name) {
        params = new Proxy(new URLSearchParams(window.location.search), {
          get: (searchParams, prop) => searchParams.get(prop),
        });
        var url = document.location.pathname;
        var inserted = false;
        ['org', 'comp'].forEach(function(element, index){
          prefix = index && inserted ? '&' : '?';
          if (element == name) {
            url += prefix + name + '=' + el.val();
            inserted = true;
          }
          else {
            if (params[element] != null) {
              url += prefix + name + '=' + params[element];
              inserted = true;
            }
          }
        });
        window.history.pushState("", "", url);
      }
      $(".cena, .kolichestvo, .skidka_procent, .skidka_summa, .nacenka_procent, .nacenka_summa, .edinica_izmereniya, .nds, .reg_nomer, .kolichestvo_proslezh").change(function(){
        recalc();
      });
      $(".repeater-wrapper .delete-row").click(function(){
        $(this).closest(".repeater-wrapper").hide();
        $(this).closest(".repeater-wrapper").find("input.kolichestvo").val(0);
        $(this).closest(".repeater-wrapper").find("input.uit").val('');
      });
      $(".peremeshenie_tovarov_form .kolichestvo_na_rukah").change(function(){
        var dostupno = parseFloat($(".kolichestvo_dostupno").val());
        var novoe_kolichestvo = parseFloat($(this).val());
        $(".kolichestvo").val(novoe_kolichestvo - dostupno);
      });
      $(".peremeshenie_tovarov_form .kolichestvo").change(function(){
        var dostupno = parseFloat($(".kolichestvo_dostupno").val());
        var izmenenie = parseFloat($(this).val());
        $(".kolichestvo_na_rukah").val(dostupno + izmenenie);
      });
      $(".harakteristika").unbind('change').change(function() {
        var line = $(this).closest(".repeater-wrapper");
        if (line.find("input.cena").length > 0) {
          $.ajax({
            url: '/tovar/harakteristika-cena',
            data: {
              harakteristika: $(this).val(),
              tovar: line.find("input.uit").val()
            },
            success: function (data) {
              line.find("input.cena").val(data.cena).trigger("change");
              $(".field-add-more-submit").show();
            }
          });
        }
        if (line.find("input.kolichestvo_dostupno").length > 0) {
          var sklad = $(".pole_sklada").val();
          if (sklad == '_none') {
            alert(Drupal.t('Select warehouse'));
            return;
          }

          $.ajax({
            url: '/tovar/harakteristika-ostatok',
            data: {
              harakteristika: $(this).val(),
              tovar: line.find("input.uit").val(),
              sklad: sklad
            },
            success: function (data) {
              line.find("input.kolichestvo_dostupno").val(data.ostatok);
              line.find("input.kolichestvo_na_rukah").val('');
              line.find("input.kolichestvo").val('');
              $(".field-add-more-submit").show();
            }
          });
        }
      });
      $(".uit").on('autocompleteclose', function(event, node) {
        var line = $(this).closest(".repeater-wrapper");
        if (line.find("input.kolichestvo_dostupno").length > 0) {
          var sklad = $(".pole_sklada").val();
          if (sklad == '_none') {
            alert(Drupal.t('Select warehouse'));
            return;
          }

          $.ajax({
            url: '/tovar/harakteristika-ostatok',
            data: {
              harakteristika: line.find("select.harakteristika").val(),
              tovar: line.find("input.uit").val(),
              sklad: sklad
            },
            success: function (data) {
              line.find("input.kolichestvo_dostupno").val(data.ostatok);
              line.find("input.kolichestvo_na_rukah").val('');
              line.find("input.kolichestvo").val('');
              $(".field-add-more-submit").show();
            }
          });
        }
      });
      $('.repeater-wrapper .uit').on('autocompleteselect', function(event, node) {
        $(".field-add-more-submit").hide();
        var line = $(this).closest(".repeater-wrapper");
        if (line.find("input.kolichestvo").val() == '') {
          if ($("form.peremeshenie_tovarov_form").length == 0)
            line.find("input.kolichestvo").val(1);
        }
        if (node.item.cena == undefined) {
          var hselect = line.find('select.harakteristika');
          hselect.html('');
          $.each(node.item.harakteristiki, function (index, value) {
            hselect.append(`<option value="${index}">${value}</option>`);
          });
          setTimeout(function(){
            hselect.trigger("change");
          }, 500)
          $(".field-add-more-submit").show();
        }
        else {
          line.find("input.cena").val(node.item.cena).trigger("change");
          $(".field-add-more-submit").show();
        }
        line.find("input.opisanie").val(node.item.opisanie);
        line.find("select.nds").val(node.item.nds).trigger("change");
        if (node.item.skidka)
          line.find("input.skidka_procent").val(node.item.skidka).trigger("change");

        var edinica_izmereniya = line.find("select.edinica_izmereniya").val(node.item.edinica_izmereniya_id);
        if (edinica_izmereniya)
          $("." + edinica_izmereniya.attr("id")).html(edinica_izmereniya.find("option:selected").html());
      });
      $("select[data-drupal-selector='edit-field-tp-organizaciya']").change(function(el, a, b){
        $("select[data-drupal-selector='edit-field-tp-schet-v-banke']").val("_none");
        updateSchetVBankeField($(this).val());
      });

      updateSchetVBankeField($("select[data-drupal-selector='edit-field-tp-organizaciya']").val());

      function updateSchetVBankeField(val) {
        var allowed = [];
        $.each(settings.org_scheta, function(e, el){
          if (el.field_tp_organizaciya_target_id == val)
            allowed.push(el.entity_id)
        });

        $("select[data-drupal-selector='edit-field-tp-schet-v-banke'] option").each(function(){
          if ($(this).val() != '_none' && allowed.indexOf($(this).val()) == -1)
            $(this).attr('disabled', 'disabled');
          else
            $(this).removeAttr('disabled');
        });
      }
      function recalc() {
        var skidka_itogo = 0,
          nacenka_itogo = 0,
          itogo = 0;
        $(".repeater-wrapper").each(function(){
          res = calculate_line_total($(this));
          itogo = itogo + res.itogo;
          skidka_itogo = skidka_itogo + res.skidka;
          $(this).closest(".repeater-wrapper").find(".amount").html(res.itogo + res.skidka);
        });
        $("#itogo").html(itogo);
        $("#skidka").html(skidka_itogo);
      }
      function calculate_line_total(line) {
        var edinica_izmereniya = line.find("select.edinica_izmereniya");
        if (edinica_izmereniya)
          $("." + edinica_izmereniya.attr("id")).html(edinica_izmereniya.find("option:selected").html());

        var nds = line.find("select.nds");
        if (nds) {
          $("." + nds.attr("id")).html(nds.find("option:selected").html());
          nds_val = nds.val();
          if (isNaN(nds_val * 1)) {
            nds_val = 0;
          }
        }

        var skidka_procent = line.find("input.skidka_procent");
        if (skidka_procent) {
          $("." + skidka_procent.attr("id")).html(skidka_procent.val());
          if (skidka_procent.val() > 0)
            line.find(".skidka_procent_wrp").show();
          else
            line.find(".skidka_procent_wrp").hide();
        }

        var skidka_summa = line.find("input.skidka_summa");
        if (skidka_summa) {
          $("." + skidka_summa.attr("id")).html(skidka_summa.val());
          if (skidka_summa.val() > 0)
            line.find(".skidka_summa_wrp").show();
          else
            line.find(".skidka_summa_wrp").hide();
        }

        var nacenka_procent = line.find("input.nacenka_procent");
        if (nacenka_procent) {
          $("." + nacenka_procent.attr("id")).html(nacenka_procent.val());
          if (nacenka_procent.val() > 0)
            line.find(".nacenka_procent_wrp").show();
          else
            line.find(".nacenka_procent_wrp").hide();
        }

        var nacenka_summa = line.find("input.nacenka_summa");
        if (nacenka_summa) {
          $("." + nacenka_summa.attr("id")).html(nacenka_summa.val());
          if (nacenka_summa.val() > 0)
            line.find(".nacenka_summa_wrp").show();
          else
            line.find(".nacenka_summa_wrp").hide();
        }

        var reg_nomer = line.find("input.reg_nomer");
        if (reg_nomer) {
          $("." + reg_nomer.attr("id")).html(reg_nomer.val());
          if (reg_nomer.val() != '')
            line.find(".reg_nomer_wrp").show();
          else
            line.find(".reg_nomer_wrp").hide();
        }

        var kolichestvo_proslezh = line.find("input.kolichestvo_proslezh");
        if (kolichestvo_proslezh) {
          $("." + kolichestvo_proslezh.attr("id")).html(kolichestvo_proslezh.val());
          if (kolichestvo_proslezh.val() != '')
            line.find(".kolichestvo_proslezh_wrp").show();
          else
            line.find(".kolichestvo_proslezh_wrp").hide();
        }

        var edinica_izmereniya = line.find("select.edinica_izmereniya");
        if (edinica_izmereniya) {
          // $("." + edinica_izmereniya.attr("id")).html(edinica_izmereniya.val());
          if (edinica_izmereniya.val() != '')
            line.find(".edinica_izmereniya_wrp").show();
          else
            line.find(".edinica_izmereniya_wrp").hide();
        }

        var total = parseFloat(line.find(".cena").val()) * parseFloat(line.find(".kolichestvo").val());
        var skidka_procent = (total / 100) * parseFloat(line.find(".skidka_procent").val());
        var nacenka_procent = (total / 100) * parseFloat(line.find(".nacenka_procent").val());
        if (isNaN(skidka_procent))
          skidka_procent = 0;
        if (isNaN(nacenka_procent))
          nacenka_procent = 0;
        var skidka_summa = parseFloat(line.find(".skidka_summa").val());
        var nacenka_summa = parseFloat(line.find(".nacenka_summa").val());
        if (isNaN(skidka_summa))
          skidka_summa = 0;
        if (isNaN(nacenka_summa))
          nacenka_summa = 0;
        total = total - skidka_procent - skidka_summa + nacenka_procent + nacenka_summa;
        if (nds_val)
          total = total + (total / 100) * nds_val;
        if (isNaN(total))
          total = '';
        else
          total = total.toFixed(2);
        line.find(".itogo").val(total);
        line.find(".total").html(total);
        return {
          itogo: total == '' ? 0 : parseFloat(total),
          skidka: skidka_summa + skidka_procent,
          nacenka: nacenka_summa + nacenka_procent,
        };
      }
    }
  };
})(jQuery, Drupal);
